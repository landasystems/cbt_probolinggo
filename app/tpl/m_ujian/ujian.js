app.controller('appujianCtrl', function ($state, $scope, Data, toaster, Upload) {
    //init data
    var tableStateRef;
    var Control_link = "appujian";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.idKelas = '';
    Data.get('appujian/listmateri').then(function (data) {
        $scope.materi = data.data;
    });
    Data.get('appujian/listKelas').then(function (data) {
        $scope.listKelas = data.data;
    });

    $scope.changeKodeUjian = function (val) {
        Data.get(Control_link + '/getKodeUjian/' + val).then(function (response) {
            if (response.status == 1) {
                toaster.pop('success', "Berhasil", "Data berhasil tersimpan");
            } else {
                toaster.pop('error', "Terjadi Kesalahan", "Kode Ujian ");
            }
        });
    }

    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {offset: offset, limit: limit};

        if (tableState.sort.predicate) {
            param['sort'] = tableState.sort.predicate;
            param['order'] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param['filter'] = tableState.search.predicateObject;
        }

        Data.get(Control_link + '/index', param).then(function (data) {
            $scope.displayed = data.data;
            tableState.pagination.numberOfPages = Math.ceil(data.totalItems / limit);
        });

        $scope.isLoading = false;
    };

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.type_penilaian = 'reguler';
        $scope.form.status = '0';
        $scope.form.is_acak = '1';
        $scope.detUjian = [
            {
                id_materi: '',
                level: '',
                jumlah_soal: ''
            }];
        $scope.detUjian2 = [
            {
                nama: '',
                nip: '',
                no_ujian: '',
                j_kelamin: '',
                tempat_lahir: '',
                tanggal_lahir: ''
            }];
    };

    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Edit Data : " + form.nama_ujian;
        $scope.form = form;
        $scope.detail(form.id);
        $scope.detail2(form.id);
//        $scope.totalMaxs();
    };

    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtitle = "Lihat Data : " + form.nama_ujian;
        $scope.form = form;
        $scope.detail(form.id);
        $scope.detail2(form.id);
    };

    $scope.detail = function (id) {
        Data.get('appujian/listdetail/' + id).then(function (data) {
            $scope.detUjian = data.data;
            $scope.totalMaxs();
        });
    };

    $scope.detail2 = function (id) {
        Data.get('appujian/listdetail2/' + id).then(function (data) {
            $scope.detUjian2 = data.data;
        });
    };

    $scope.save = function (form, details, details2) {
        console.log(details)
        var data = {
            form: form,
            detail: details,
            detail2: details2
        };
//        console.log(data);
        var url = (form.id > 0) ? '/update' : '/create/';
        Data.post(Control_link + url, data).then(function (result) {
            if (result.status == 0) {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            } else {
                if (result.status == 2) {
                    toaster.pop('error', "Terjadi Kesalahan", result.errors);
                } else if (result.status == 3) {
                    toaster.pop('error', "Terjadi Kesalahan", result.errors);
                } else {
                    $scope.is_edit = false;
                    $scope.callServer(tableStateRef); //reload grid ulang
                    toaster.pop('success', "Berhasil", "Data berhasil tersimpan");
                    $scope.idKelas = '';
                }
            }
        });
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.callServer(tableStateRef);
    };

    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANENT item ini ?")) {
            Data.delete(Control_link + '/delete/' + row.id).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.totalMaxs = function () {
        var total = 0;
        var totalsoal = 0;
        angular.forEach($scope.detUjian, function (detail) {
            var jml = (detail.jumlah_soal) ? parseInt(detail.jumlah_soal) : 0;
            var nilai_benar = (detail.nilai_benar) ? parseFloat(detail.nilai_benar) : 0;
            detail.nilai_max = (jml * nilai_benar);
            total += detail.nilai_max;
            totalsoal += jml;
        })
        $scope.form.total = total;
        $scope.form.totalsoal = totalsoal;
    }

    $scope.totalMax = function (index, pertama) {
        if ($scope.detUjian[index].jumlah_soal > $scope.detUjian[index].jumlah_soal_asli) {
            toaster.pop('error', "Terjadi Kesalahan", "Jumlah Soal Melebihi Batas");
            $scope.detUjian[index].jumlah_soal = $scope.detUjian[index].jumlah_soal_asli;
        } else {
            $scope.totalMaxs();
        }
    }

    $scope.nilaiSoal = function (list_materi, index) {
//        var id_materi = "2";
        if (list_materi.nilai_benar == null) {
            var data = [
                (list_materi.nilai_a) ? parseInt(list_materi.nilai_a) : 0,
                (list_materi.nilai_b) ? parseInt(list_materi.nilai_b) : 0,
                (list_materi.nilai_c) ? parseInt(list_materi.nilai_c) : 0,
                (list_materi.nilai_d) ? parseInt(list_materi.nilai_d) : 0,
                (list_materi.nilai_e) ? parseInt(list_materi.nilai_e) : 0,
            ];

            $scope.detUjian[index].nilai_benar = Math.max.apply(Math, data);
            $scope.detUjian[index].nilai_salah = list_materi.nilai_salah;
        } else {
            $scope.detUjian[index].nilai_benar = list_materi.nilai_benar;
            $scope.detUjian[index].nilai_salah = list_materi.nilai_salah;
        }

        $scope.detUjian[index].nilai_a = list_materi.nilai_a;
        $scope.detUjian[index].nilai_b = list_materi.nilai_b;
        $scope.detUjian[index].nilai_c = list_materi.nilai_c;
        $scope.detUjian[index].nilai_d = list_materi.nilai_d;
        $scope.detUjian[index].nilai_e = list_materi.nilai_e;
        $scope.detUjian[index].jumlah_soal_asli = data.data;
    }

    $scope.jumlah = function (id, level, index) {
        Data.get('appujian/jumlah_soal/' + id + "/" + level).then(function (data) {
            $scope.detUjian[index].jumlah_soal = data.data;
            $scope.detUjian[index].jumlah_soal_asli = data.data;
            $scope.totalMaxs();
        });
    }

//    detail Tambah Materi
    $scope.addDetail = function () {
        var newDet = {
            uraian: '',
            harga: '0'
        }
        $scope.detUjian.push(newDet);
    };

    $scope.removeRow = function (paramindex, id) {

        if (id.length != 0) {
            Data.delete('appujian/deleteDetail/' + id).then(function (data) {
                toaster.pop('success', "Berhasil", "Materi berhasil Dihapus");
            });
        }
        var comArr = eval($scope.detUjian);
        if (comArr.length > 1) {
            $scope.detUjian.splice(paramindex, 1);
            $scope.total();
        } else {
            alert("Something gone wrong");
        }

    };

//    detail Tambah Peserta

    $scope.updatePeserta = function (id) {
        if (id == undefined || id == '') {
            toaster.pop('error', "Terjadi Kesalahan!", "Anda Belum Memilih Kelas");
        } else {
            Data.get('appujian/updatePeserta/' + id).then(function (data) {
                $scope.detUjian2 = data.data;
            });
        }
    }

    $scope.addDetail2 = function () {
        var newDet = {
            nama: '',
            nip: '',
            no_ujian: '',
            j_kelamin: '',
            tempat_lahir: '',
            tanggal_lahir: ''
        }
        $scope.detUjian2.push(newDet);
    };

    $scope.removeRow2 = function (paramindex, id) {

        if (id.length != 0) {
            Data.delete('appujian/deletePeserta/' + id).then(function (data) {
                toaster.pop('success', "Berhasil", "Peserta berhasil Dihapus");
            });
        }

        var comArr = eval($scope.detUjian2);
        if (comArr.length > 1) {
            $scope.detUjian2.splice(paramindex, 1);
        } else {
            alert("Something gone wrong");
        }

    };

    $scope.setstatus = function () {
        $scope.opened1 = -1;
    }

    $scope.open1 = function ($event, $index) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = $index;
    };

//    import data peserta
    $scope.uploadFiles = function (file, errFiles) {
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
//            console.log(file);
            file.upload = Upload.upload({
//                url: 'http://cbt.sunsal.or.id/appujian/upload',
                url: '../appujian/upload',
                data: {file: file}
            });
            file.upload.then(function (response) {
                console.log(response.data);
                if ($scope.detUjian2.length == 1) {
                    $scope.detUjian2 = response.data.data;
                } else {
                    var lama = $scope.detUjian2;
                    var baru = response.data.data;
//                console.log(response.data.data);
                    var jadi = lama.concat(baru);
                    $scope.detUjian2 = jadi;
//                console.log(jadi);
//                $scope.detUjian = lama;
//                $scope.total();
                }
                console.log($scope.detUjian2);

                toaster.pop('success', "Berhasil", "Data Berhasil Di Import");
                $scope.callServer(tableStateRef); //reload grid ulang

            });
        } else {
            toaster.pop('error', "Terjadi Kesalahan", result.errors);
        }
    };
})
