app.controller('hasilCtrl', function ($scope, $state, $stateParams, Data, toaster, $timeout) {
    $scope.tampilkan = false;
    $scope.data = {};
    $scope.form = {};
    $scope.form.mode = "materi";

    $scope.exportDataPPDB = function () {
        window.open('../apphasil/export', '_blank');
    }

    Data.get('site/getLogo').then(function (data) {
        $scope.desk = data.data;

    });

//    console.log($stateParams.id_ujian);

    Data.get('apphasil/listkode_ujian').then(function (data) {
        $scope.kode_ujian = data.data;
    });

    if ($stateParams.id_ujian != undefined) {
//        console.log("yes");
        $scope.is_live = true;
        Data.get('apphasil/view/' + $stateParams.id_ujian).then(function (data) {
            $scope.form.kode_ujian = data.data;
            var formm = $scope.form;
            $scope.view(formm);
        });
        $scope.onTimeout = function () {
            var form = $scope.form;
            $scope.view(form);
            mytimeout = $timeout($scope.onTimeout, 10000);

        }
        var mytimeout = $timeout($scope.onTimeout, 10000);
    }


    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('laporan').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Laporan-Kas.xls");
    };
    $scope.open1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    };
    $scope.getAkun = function (idUnker) {
        Data.get('appakun/akunkas/' + idUnker).then(function (data) {
            $scope.lAkun = data.data;
        });
    };
    $scope.view = function (form) {
//        console.log(form);
        Data.post('apphasil/laporan/', form).then(function (data) {
            if (data.status == 0) {

                toaster.pop('error', "Terjadi Kesalahan", data.errors);
            } else {

                if (data.peserta.length == 0) {
                    $scope.tampilkan = false;
                    toaster.pop('error', "Terjadi Kesalahan", "Belum Ada Peserta yang Ujian");
                } else {
                    $scope.date = new Date();
                    $scope.ujian = data.ujian;
                    $scope.kolom = data.kolom;
                    $scope.peserta = data.peserta;
                    $scope.kelompok = data.kelompok;
                    $scope.tampilkan = true;
                }
            }
        });
    };

    $scope.printDiv = function (divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    $scope.print = function () {
        window.open('../appujian/exportexcel');
    };

    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('laporan').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Laporan-hasil-ujian.xls");
    };



});