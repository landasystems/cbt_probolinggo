app.controller('anabutCtrl', function ($scope, $state, $stateParams, Data, toaster, $timeout) {
    $scope.tampilkan = false;
    $scope.data = {};
    $scope.form = {};

    Data.get('site/getLogo').then(function (data) {
        $scope.desk = data.data;
    });

    Data.get('appanabut/listmateri').then(function (data) {
        $scope.materi = data.data;
    });

    $scope.view = function (form) {

        if (form == undefined) {
            toaster.pop('error', "Terjadi Kesalahan", "Anda Belum Memilih Materi");
        } else {
            Data.get('appanabut/laporan/' + form.id).then(function (data) {
                if (data.status == 0) {
                    toaster.pop('error', "Terjadi Kesalahan", data.errors);
                } else {
                    if (data.data[0].peserta.length == 0) {
                        toaster.pop('error', "Terjadi Kesalahan", "Belum Ada yang Ujian Pada Materi Ini");
                    } else {
                        console.log(data)
                        $scope.keterangan = form;
                        $scope.date = new Date();
                        $scope.anabut = data.data;
                        $scope.tampilkan = true;
                    }
                }
            });
        }
    };

    $scope.printDiv = function (divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    $scope.exportDataPPDB = function () {
        window.open('../apphasil/export', '_blank');
    };

});