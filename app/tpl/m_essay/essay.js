app.controller('essayCtrl', function ($scope, Data, toaster, $modal) {
    //init data
    var tableStateRef;
    var Control_link = "appessay";
    $scope.displayed = [];
    $scope.is_view = false;
    $scope.idUjian = '';

    Data.get(Control_link + '/getUjian').then(function (data) {
        $scope.ujian = data.data;
    });

    $scope.view = function (id) {
        if (id == undefined || id == '') {
            toaster.pop('error', "Terjadi Kesalahan", "Anda Belum Memilih Kelas!");
        } else {
            $scope.idUjian = id;
            $scope.callServer(tableStateRef);
            $scope.is_view = true;
        }
    };

    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var idUjian = $scope.idUjian;
        var param = {offset: offset, limit: limit, id_ujian: idUjian};

        if (tableState.sort.predicate) {
            param['sort'] = tableState.sort.predicate;
            param['order'] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param['filter'] = tableState.search.predicateObject;
        }

        Data.get(Control_link + '/index', param).then(function (data) {
            if (data.totalItems == 0 && $scope.idUjian != '') {
                $scope.is_view = false;
                toaster.pop('error', "Terjadi Kesalahan", "Data Kosong");
            } else {
                $scope.displayed = data.data;
            }

            tableState.pagination.numberOfPages = Math.ceil(data.totalItems / limit);
        });

        $scope.isLoading = false;
    };

    $scope.listJawaban = function (id, nama) {
        Data.get(Control_link + '/getJawaban/' + id).then(function (data) {
            if (data.status == 0) {
                toaster.pop('error', "Terjadi Kesalahan", "Tidak ada jawaban essay pada peserta dengan nama " + nama);
            } else {
                data.data.nama = nama;
                data.data.idTes = id;
                var modalInstance = $modal.open({
                    templateUrl: 'tpl/m_essay/modal.html',
                    controller: 'modalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        form: function () {
                            return data.data;
                            console.log(data.data)
                        }
                    }
                });
                modalInstance.result.then(function (result) {
//                $scope.isiData = result;
                }, function () {

                })
            }
        });
    };


});
app.controller('modalCtrl', function ($state, $scope, toaster, Data, $modalInstance, form) {
//    console.log(form);
    $scope.formmodal = form;
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.reloadJawaban = function () {
        Data.get('appessay/getJawaban/' + form.idTes).then(function (data) {
            $scope.formmodal = data.data;
//            console.log(data.data)
        });
    }

    $scope.update = function (id, isi) {
        Data.get('appessay/updateEssay/' + id + '/' + isi).then(function (data) {
            toaster.pop('success', "Berhasil", "Data Berhasil di Update");
            $scope.reloadJawaban();
        });
    }

    $scope.save = function (form) {
//        console.log(form);
        var url = 'form/create';
        Data.post(url, form).then(function (result) {
            if (result.status == 0) {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            } else {
                $modalInstance.close(result.data);
                $state.go('site.detail', {id: result.data.no});
            }
        });
    };
});
