app.controller('appmateriCtrl', function ($state, $scope, Data, toaster) {
    //init data
    var tableStateRef;
    var Control_link = "appmateri";
    $scope.displayed = [];
    $scope.form = {};

    $scope.is_edit = false;
    $scope.is_view = false;
    Data.get('appmateri/listkelompok').then(function (data) {
        $scope.kelompok = data.data;
    });

    Data.get(Control_link + '/getTahun').then(function (data) {
        $scope.tahun = data.data;
    });

    Data.get('appkelompok/index').then(function (data) {
        $scope.dataKelas = data.data;
    });




    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {offset: offset, limit: limit};

        if (tableState.sort.predicate) {
            param['sort'] = tableState.sort.predicate;
            param['order'] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param['filter'] = tableState.search.predicateObject;
        }

        Data.get(Control_link + '/index', param).then(function (data) {
            $scope.displayed = data.data;
            tableState.pagination.numberOfPages = Math.ceil(data.totalItems / limit);
        });

        $scope.isLoading = false;
    };

    $scope.changeAprove = function (form) {
        Data.post(Control_link + '/setAprove',form).then(function (data) {
            console.log(data);
            if (data.data.is_aprove == 1){
                toaster.pop('success', "Berhasil", "Data berhasil di aprove");
            }else{
                toaster.pop('warning', "Berhasil", "Data berhasil di batalkan");
            }
            $scope.callServer(tableStateRef); //reload grid ulang
        });
    }

//    $scope.pindahsoal = function (row, val) {
//        var dataSession = {
//            level: val,
//            id_kelompok: row.id_kelompok,
//            id_materi: row.id
//        };
//
//        Data.post(Control_link + '/session_soal', dataSession).then(function (result) {
//            $state.go('master.soal', {form: 'data'});
//        });
//    };

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.cara_penilaian = '1';
        $scope.form.type_jawaban = "e";
        $scope.form.type_penilaian = "reguler";
//        $scope.form.tahun = '2016';
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Edit Data : " + form.nama;

        $scope.form = form;
    };

    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtitle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.select_kelompok(form.id_kelompok);
    };
    $scope.save = function (form) {

        var url = (form.id > 0) ? '/update' : '/create/';
        Data.post(Control_link + url, form).then(function (result) {
            if (result.status == 0) {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            } else {
//                $scope.is_create = true;
                $scope.is_edit = false;
                $scope.callServer(tableStateRef); //reload grid ulang
                toaster.pop('success', "Berhasil", "Data berhasil tersimpan");
            }
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.callServer(tableStateRef);
    };

    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANENT item ini ?")) {
            Data.delete(Control_link + '/delete/' + row.id).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };


})
