app.controller('appsoalCtrl', function ($state, $stateParams, $scope, Data, toaster, Upload) {
        //init data
        var tableStateRef;
        var Control_link = "appsoal";
        $scope.displayed = [];
        $scope.form = {};
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtitle = "Form Tambah Data";
        $scope.form.css = "btn-success";

        Data.get('appsoal/listnomor/' + $stateParams.id_materi + '/' + $stateParams.level).then(function (data) {
            $scope.listnomor = data.data;
            $scope.type_soal = data.cara_penilaian;
            $scope.form.type_soal = data.cara_penilaian;
            $scope.type_jawaban = data.type_jawaban;
        });

        $scope.detSoal = [
            {
                id_materi: $stateParams.id_materi,
                level: $stateParams.level,
                no: 1
            }
        ];

        $scope.addDetail = function (no) {
            var newDet = {
                id_materi: $stateParams.id_materi,
                level: $stateParams.level,
                no: no + 1
            }
            $scope.detSoal.push(newDet);
        };

        $scope.removeRow = function (paramindex, id, idSoalCerita, formId) {

            if (id != undefined) {
                if (confirm("Apa anda yakin akan MENGHAPUS PERMANENT item  ?")) {
                    Data.delete(Control_link + '/delete/' + id).then(function (result) {
                        if (result.status == 0) {
                            toaster.pop('error', "Terjadi Kesalahan", result.errors);
                        } else {
                            Data.get('appsoal/listnomor/' + $stateParams.id_materi + '/' + $stateParams.level).then(function (data) {
                                $scope.listnomor = data.data;
                                $scope.selects(formId);
                            });
                            Data.get('appsoal/getSoalCerita/' + idSoalCerita).then(function (data) {
                                $scope.detSoal = data.data;
                            });
                            toaster.pop('success', "Berhasil", "Data berhasil dihapus");


                        }
                    });
                }
                ;
            }

            var comArr = eval($scope.detSoal);
            if (comArr.length > 1) {
                $scope.detSoal.splice(paramindex, 1);
            } else {
                alert("Something gone wrong");
            }

        };

        $scope.kembali = function () {
            $state.go('master.materi', {form: 'data'});
        };

        $scope.list = function (form) {
            $scope.is_edit = false;
            $scope.view = false;
        };

        $scope.create = function (form) {
            $scope.is_edit = true;
            $scope.is_view = false;
            $scope.formtitle = "Form Tambah Data";
            $scope.form = {};
            $scope.selects(0);
            $scope.form.css = "btn-success";
            $scope.form.type_soal = $scope.type_soal;
            $scope.detSoal = [
                {
                    id_materi: $stateParams.id_materi,
                    level: $stateParams.level,
                    no: 1
                }
            ];
        };
        $scope.update = function (form) {
            console.log(form);
            if (form.type_soal == 3) {
                // Data.get('appsoal/getCerita/' + form.id_soal_cerita).then(function (data) {
                //     form.soal_cerita = data.data.soal_cerita;
                // });
                // form.soal_cerita = data.cerita.soal_cerita;
                Data.get('appsoal/getSoalCerita/' + form.id_soal_cerita).then(function (data) {
                    $scope.detSoal = data.data;
                    // form.soal_cerita = data.cerita.soal_cerita;
                    $scope.idSoalCerita = data.data.id;
                });
                $scope.selects(form.id);
                $scope.is_edit = true;
                $scope.is_view = false;
                $scope.formtitle = "Edit Soal No : " + form.no;
                $scope.form = form;
                $scope.form.css = "btn-primary";
                $scope.form.idSoal = form.id
            } else {
                $scope.selects(form.id);
                $scope.is_edit = true;
                $scope.is_view = false;
                $scope.formtitle = "Edit Soal No : " + form.no;
                $scope.form = form;
                $scope.form.css = "btn-primary";
                $scope.form.idSoal = form.id;
                $scope.detSoal = undefined;
            }
        };
        $scope.selects = function (id) {
            angular.forEach($scope.listnomor, function (value) {
                if (value.id == id) {
                    value.cssstyle = "btn-success";
                } else {
                    value.cssstyle = "btn-primary";
                }
            });
        };

        $scope.view = function (form) {
            $scope.is_edit = true;
            $scope.is_view = true;
            $scope.formtitle = "Lihat Soal No : " + form.no;
            $scope.form = form;
            $scope.select_kelompok(form.id_kelompok);
        };
        $scope.save = function (form, detail) {

            var data = {
                'form': form,
                'detail': detail
            }

            if (form.type_soal == 3) {
                $scope.form.id_soal_cerita = $scope.idSoalCerita;
            }

            $scope.form.id_kelompok = $stateParams.id_kelompok;
            $scope.form.id_materi = $stateParams.id_materi;
            $scope.form.level = $stateParams.level;
            var url = (form.id > 0) ? '/update' : '/create/';
            Data.post(Control_link + url, data).then(function (result) {
                if (result.status == 0) {
                    toaster.pop('error', "Terjadi Kesalahan", result.errors);
                } else {
                    Data.get('appsoal/listnomor/' + $stateParams.id_materi + '/' + $stateParams.level).then(function (data) {
                        $scope.listnomor = data.data;
                    });
                    $scope.formtitle = "Form Tambah Data";
                    $scope.form = {};
                    $scope.form.type_soal = $scope.type_soal;
                    $scope.detSoal = [
                        {
                            id_materi: $stateParams.id_materi,
                            level: $stateParams.level,
                            no: 1
                        }
                    ];

                    toaster.pop('success', "Berhasil", "Data berhasil tersimpan");
                    $scope.form.css = "btn-success";
                }
            });
        };
        $scope.cancel = function () {
            $scope.is_edit = false;
            $scope.is_view = false;
            $scope.callServer(tableStateRef);
        };

        $scope.delete = function (row, detail, idSoalCerita) {
            if (confirm("Apa anda yakin akan MENGHAPUS PERMANENT item  ?")) {

                if (detail == undefined) {
                    Data.delete(Control_link + '/delete/' + row).then(function (result) {
                        if (result.status == 0) {
                            toaster.pop('error', "Terjadi Kesalahan", result.errors);
                        } else {
                            Data.get('appsoal/listnomor/' + $stateParams.id_materi + '/' + $stateParams.level).then(function (data) {
                                $scope.listnomor = data.data;
                            });
                            $scope.formtitle = "Form Tambah Data";
                            $scope.form = {};
                            toaster.pop('success', "Berhasil", "Data berhasil dihapus");
                            $scope.form.css = "btn-success";
                            $scope.form.type_soal = $scope.type_soal;
                        }
                    });
                } else {
                    var data = {
                        detail: detail,
                        idSoalCerita: idSoalCerita
                    };
                    Data.post(Control_link + '/deleteSemua', data).then(function (result) {
                        if (result.status == 0) {
                            toaster.pop('error', "Terjadi Kesalahan", result.errors);
                        } else {
                            Data.get('appsoal/listnomor/' + $stateParams.id_materi + '/' + $stateParams.level).then(function (data) {
                                $scope.listnomor = data.data;
                            });
                            $scope.formtitle = "Form Tambah Data";
                            $scope.form = {};
                            toaster.pop('success', "Berhasil", "Data berhasil dihapus");
                            $scope.form.css = "btn-success";
                            $scope.form.type_soal = $scope.type_soal;
                        }
                    });
                }

                $scope.detSoal = [
                    {
                        id_materi: $stateParams.id_materi,
                        level: $stateParams.level,
                        no: 1
                    }
                ];


                Data.get('appsoal/listnomor/' + $stateParams.id_materi + '/' + $stateParams.level).then(function (data) {
                    $scope.listnomor = data.data;
                });
            }
        };

        $scope.export = function () {
            window.open('../appsoal/export', '_blank');
        };

        $scope.editorOptions = {
            height: "125px"
        };

        $scope.editorOptionsJawaban = {
            height: "80px",
            toolbar: "MyToolbar",
            toolbar_MyToolbar: [
                ['Image', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'insert', '-', 'RemoveFormat', 'NumberedList'],
                ['BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr'],
                ['BidiRtl','Styles','Format','Font','FontSize','TextColor','BGColor'],

            ],
            removePlugins: 'elementspath',
            resize_enabled: false
        };
        $scope.uploadFiles = function (file, errFiles) {
            $scope.f = file;
            $scope.errFile = errFiles && errFiles[0];
            if (file) {
                file.upload = Upload.upload({
                    url: '../appsoal/upload/' + $stateParams.id_materi + '/' + $stateParams.level,
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    Data.get('appsoal/listnomor/' + $stateParams.id_materi + '/' + $stateParams.level).then(function (data) {
                        $scope.listnomor = data.data;
                    });
                    toaster.pop('success', "Berhasil", "Data Berhasil Di Import");
                    $scope.callServer(tableStateRef); //reload grid ulang

                });
            } else {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            }
        }
        ;
    }
)
