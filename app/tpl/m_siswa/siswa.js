app.controller('pesertaCtrl', function ($scope, Data, toaster, $modal) {
    //init data
    var tableStateRef;
    var Control_link = "appsiswa";
    $scope.displayed = [];
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;

    Data.get(Control_link + '/getKelas').then(function (data) {
        $scope.kelas = data.data
    });

    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {offset: offset, limit: limit};

        if (tableState.sort.predicate) {
            param['sort'] = tableState.sort.predicate;
            param['order'] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param['filter'] = tableState.search.predicateObject;
        }

        Data.get(Control_link + '/index', param).then(function (data) {
            $scope.displayed = data.data;
            tableState.pagination.numberOfPages = Math.ceil(data.totalItems / limit);
        });

        $scope.isLoading = false;
    };

    $scope.create = function (form) {
        $scope.is_create = true;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.tanggal_lahir = new Date();
    };
    $scope.update = function (form) {
        $scope.is_create = false;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Edit Data : " + form.id;
        $scope.form = form;
    };
    $scope.view = function (form) {
        $scope.is_create = true;
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtitle = "Lihat Data : " + form.id;
        $scope.form = form;
    };

    $scope.save = function (form) {
        var url = (form.id > 0) ? 'appsiswa/update/' : 'appsiswa/create';
        Data.post(url, form).then(function (result) {
            if (result.status == 0) {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            } else {
                $scope.is_edit = false;
                $scope.callServer(tableStateRef); //reload grid ulang
                toaster.pop('success', "Berhasil", "Data berhasil tersimpan");
            }
        });
    };

    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
    };

    $scope.trash = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            row.is_deleted = 1;
            Data.post('pengguna/update/' + row.id, row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.restore = function (row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post('pengguna/update/' + row.id, row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANENT item ini ?")) {
            Data.delete('appsiswa/delete/' + row.id).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.open1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    };

    $scope.import = function () {
        var modalInstance = $modal.open({
            templateUrl: 'tpl/m_siswa/modal.html',
            controller: 'modalSiswaCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                form: function () {
                    // return data.data;
                    // console.log(data.data)
                }
            }
        });
        modalInstance.result.then(function (result) {
//                $scope.isiData = result;
            $scope.callServer(tableStateRef); //reload grid ulang
        }, function () {

        })
    }




})
app.controller('modalSiswaCtrl', function ($state, $scope, toaster, Data, $modalInstance, form, Upload) {
//    console.log(form);
    $scope.formmodal = form;
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };

    Data.get('appsiswa/getKelas').then(function (data) {
        $scope.kelas = data.data
    });

    $scope.uploadFiles = function (file, errFiles) {
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: '../appsiswa/importexcel/' + $scope.form.id_kelompok,
                data: {file: file}
            });
            file.upload.then(function (response) {
                toaster.pop('success', "Berhasil", "Data Excel Berhasil di Import");

                $scope.data_siswa = response.data;
                console.log($scope.data_siswa);
            });
        } else {
            toaster.pop('error', "Terjadi Kesalahan", result.errors);
        }
    };

    $scope.save = function (form) {
        console.log(form)
        Data.post('appsiswa/saveImport', form).then(function (result) {
            if (result.status == 1) {
                $modalInstance.close(result.data);
            } else {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            }
        });
    };
});