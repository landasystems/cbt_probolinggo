angular.module('app').run(
    ['$rootScope', '$state', '$stateParams', 'Data',
        function ($rootScope, $state, $stateParams, Data) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            //pengecekan login
            $rootScope.$on("$stateChangeStart", function (event, toState) {
                Data.get('site/session').then(function (results) {
                    if (typeof results.data.user != "undefined") {
                        $rootScope.user = results.data.user;
                    } else {
                        $state.go("access.signin");
                    }
                });
            });
        }
    ]).config(
    ['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/site/dashboard');
            $stateProvider.state('site', {
                abstract: true,
                url: '/site',
                templateUrl: 'tpl/app.html'
            }).state('site.dashboard', {
                url: '/dashboard',
                templateUrl: 'tpl/dashboard.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load('tpl/site/dashboard.js');
                        }
                    ]
                }
            })
            // others
                .state('access', {
                    url: '/access',
                    template: '<div ui-view class="fade-in-right-big smooth"></div>'
                }).state('access.signin', {
                url: '/signin',
                templateUrl: 'tpl/page_signin.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load('tpl/site/site.js').then();
                        }
                    ]
                }
            }).state('access.404', {
                url: '/404',
                templateUrl: 'tpl/page_404.html'
            }).state('access.forbidden', {
                url: '/forbidden',
                templateUrl: 'tpl/page_forbidden.html'
            })
            //master
                .state('master', {
                    url: '/master',
                    templateUrl: 'tpl/app.html'

                }).state('master.penggunaprofile', {
                url: '/penggunaprofile',
                templateUrl: 'tpl/m_user/profile.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load('tpl/m_user/pengguna_profile.js');
                        }
                    ]
                }
            }).state('master.kelas', {
                url: '/kelas',
                templateUrl: 'tpl/m_kelompok/index.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load('tpl/m_kelompok/kelompok.js');
                        }
                    ]
                }
            }).state('master.setting', {
                url: '/setting',
                templateUrl: 'tpl/setting.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load('tpl/site/setting.js');
                        }
                    ]
                }
            }).state('master.user', {
                url: '/user',
                templateUrl: 'tpl/m_user/index.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load('tpl/m_user/pengguna.js');
                        }
                    ]
                }
            }).state('master.guru', {
                url: '/guru',
                templateUrl: 'tpl/m_guru/guru.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load('tpl/m_guru/guru.js');
                        }
                    ]
                }
            }).state('master.materi', {
                url: '/materi',
                templateUrl: 'tpl/m_materi/index.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load('tpl/m_materi/materi.js');
                        }
                    ]
                }
            }).state('master.soal', {
                url: '/soal/:id_materi/:level',
                templateUrl: 'tpl/m_soal/index.html',
                controller: function ($stateParams) {
                    $stateParams.id_materi, //*** Exists! ***//
                        $stateParams.level //*** Exists! ***//
                },
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['ngFileUpload']).then(function () {
                                return $ocLazyLoad.load('tpl/m_soal/soal.js');
                            });
                        }
                    ]
                }
            }).state('master.ujian', {
                url: '/ujian',
                templateUrl: 'tpl/m_ujian/index.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['ngFileUpload']).then(function () {
                                return $ocLazyLoad.load('tpl/m_ujian/ujian.js');
                            });
                        }
                    ]
                }
            }).state('master.siswa', {
                url: '/siswa',
                templateUrl: 'tpl/m_siswa/index.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['ngFileUpload']).then(function () {
                                return $ocLazyLoad.load('tpl/m_siswa/siswa.js');
                            });
                        }
                    ]
                }

            }).state('master.essay', {
                url: '/essay',
                templateUrl: 'tpl/m_essay/index.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load('tpl/m_essay/essay.js');
                        }
                    ]
                }

            }).state('laporan.hasilujian', {
                url: '/hasilujian',
                templateUrl: 'tpl/l_hasil/index.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/l_hasil/hasil.js');
                            });
                        }
                    ]
                }
            }).state('laporan.hasilujian_stan', {
                url: '/hasilujian_stan',
                templateUrl: 'tpl/l_hasilstan/index.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/l_hasilstan/hasil.js');
                            });
                        }
                    ]
                }
            }).state('laporan.anabut', {
                url: '/anabut',
                templateUrl: 'tpl/l_anabut/anabut.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load('tpl/l_anabut/anabut.js');
                        }
                    ]
                }
            }).state('laporan.hasil', {
                url: '/hasil/:id_ujian',
                templateUrl: 'tpl/l_hasil/index.html',
                controller: function ($stateParams) {
                    $stateParams.id_ujian //*** Exists! ***//
                },
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/l_hasil/hasil.js');
                            });
                        }
                    ]
                }
//------------------------------------------------------------------------------------------

            })
            //end master
            //start transaksi
                .state('live', {
                    url: '/live',
                    templateUrl: 'tpl/blank.html'

                })
                .state('live.hasil', {
                    url: '/hasil/:id_ujian',
                    templateUrl: 'tpl/l_hasil/index.html',
                    controller: function ($stateParams) {
                        $stateParams.id_ujian //*** Exists! ***//
                    },
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                    return $ocLazyLoad.load('tpl/l_hasil/hasil.js');
                                });
                            }
                        ]
                    }
                })
                //end Transaksi
                .state('laporan', {
                    url: '/laporan',
                    templateUrl: 'tpl/app.html'
                }).state('laporan.jurnal', {
                url: '/jurnal',
                templateUrl: 'tpl/laporan/jurnal.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/laporan/jurnal.js');
                            });
                        }
                    ]
                }
            }).state('laporan.kas', {
                url: '/kas',
                templateUrl: 'tpl/laporan/kas.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/laporan/kas.js');
                            });
                        }
                    ]
                }
            }).state('laporan.bukubesar', {
                url: '/bukubesar',
                templateUrl: 'tpl/laporan/bukubesar.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/laporan/bukubesar.js');
                            });
                        }
                    ]
                }
            }).state('laporan.neraca', {
                url: '/neraca',
                templateUrl: 'tpl/laporan/neracasaldo.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/laporan/neracasaldo.js');
                            });
                        }
                    ]
                }
            }).state('laporan.hutang', {
                url: '/hutang',
                templateUrl: 'tpl/laporan/hutang.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/laporan/hutang.js');
                            });
                        }
                    ]
                }
            }).state('laporan.piutang', {
                url: '/piutang',
                templateUrl: 'tpl/laporan/piutang.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/laporan/piutang.js');
                            });
                        }
                    ]
                }
            }).state('laporan.rekaphutang', {
                url: '/rekaphutang',
                templateUrl: 'tpl/laporan/rekaphutang.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/laporan/rekaphutang.js');
                            });
                        }
                    ]
                }
            }).state('laporan.rekappiutang', {
                url: '/rekappiutang',
                templateUrl: 'tpl/laporan/rekappiutang.html',
                resolve: {
                    deps: ['$ocLazyLoad',
                        function ($ocLazyLoad) {
                            return $ocLazyLoad.load(['daterangepicker']).then(function () {
                                return $ocLazyLoad.load('tpl/laporan/rekappiutang.js');
                            });
                        }
                    ]
                }
            })
        }
    ]);
