<?php

function validasiMateri($data, $custom = array()) {
    $validasi = array(
        'id_kelompok' => 'required',
        'nama_materi' => 'required',
        'cara_penilaian' => 'required',
        'tahun' => 'required'
    );

    $cek = cek_validate($data, $validasi, $custom);
    return $cek;
}

get('/appmateri/getTahun', function() {
    $tahunSekarang = date("Y");
    $sampai = $tahunSekarang + 2;

    $models = [];
    $a = 0;
    for ($i = $tahunSekarang; $i <= $sampai; $i++) {
        $models[$a] = $i;
        $a++;
    }

    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

post('/appmateri/session_soal', function() {
    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;

    $_SESSION['soal']['id_materi'] = $data['id_materi'];
    $_SESSION['soal']['id_kelompok'] = $data['id_kelompok'];
    $_SESSION['soal']['level'] = $data['level'];

    echo json_encode(array('status' => 1, 'data' => $params), JSON_PRETTY_PRINT);
});

post('/appmateri/setAprove', function() {
    $params = json_decode(file_get_contents("php://input"), true);
    $sql = new LandaDb();
    $models = $sql->update("m_materi", array("is_aprove" => $params['is_aprove']), array("id" => $params['id']));

    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

get('/appmateri/listkelompok', function() {
    $sql = new LandaDb();
    $list = $sql->findAll("select * from m_kelompok");

    echo json_encode(array('status' => 1, 'data' => $list), JSON_PRETTY_PRINT);
});



//get('/appmateri/select/:id', function($id) {
//    $sql = new LandaDb();
//    $list = $sql->findAll("select * from m_kelompok where id='{$id}'");
//
//    echo json_encode(array('status' => 1, 'data' => $list), JSON_PRETTY_PRINT);
//});

get('/appmateri/index', function() {
//    print_r($_SESSION);
//    echo '<hr/>';
    check_access(array('admin' => true));

    $params = $_REQUEST;
    $filter = array();
    $sort = "id DESC";
    $offset = 0;
    $limit = 10;

//limit & offset pagination
    if (isset($params['limit']))
        $limit = $params['limit'];
    if (isset($params['offset']))
        $offset = $params['offset'];

//sorting
    if (isset($params['sort'])) {
        $sort = $params['sort'];
        if (isset($params['order'])) {
            if ($params['order'] == "false")
                $sort .= " ASC";
            else
                $sort .= " DESC";
        }
    }
    $all = '';

    $sql = new LandaDb();
    $sql->select("m_materi.*,m_kelompok.nama,m_user.nama as dibuat_oleh")
            ->from("m_materi")
            ->join("left join", "m_kelompok", "m_kelompok.id = m_materi.id_kelompok")
            ->join("left join", "m_user", "m_user.id = m_materi.created_by");
    if ($_SESSION['user']['roles_id'] != 0) {
        $sql->andWhere("=", "m_materi.created_by", $_SESSION['user']['id']);
    }
    $sql->limit($limit)
            ->orderBy($sort)
            ->offset($offset);

//filter
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            $sql->where('LIKE', $key, $val);
            if ($key == 'id_kelompok') {
                $sql->where('=', $key, $val);
            }
        }
    }
    $totalItem = $sql->count();

    $models = $sql->findAll();
    foreach ($models as $key => $val) {
        $models[$key] = (array) $val;

//        count soal mudah
        $sqlmudah = $sql->select("*")
                ->from("m_soal")
                ->where("=", "id_materi", $models[$key]['id'])
//                        ->andWhere("=", "id_kelompok", $models[$key]['id_kelompok'])
                ->andWhere("=", "level", 1);
        $mudah = $sqlmudah->count();

//        count soal sedang
        $sqlsedang = $sql->select("*")
                ->from("m_soal")
                ->where("=", "id_materi", $models[$key]['id'])
//                        ->andWhere("=", "id_kelompok", $models[$key]['id_kelompok'])
                ->andWhere("=", "level", 2);
        $sedang = $sqlsedang->count();

//        count soal Sulit
        $sqlsusah = $sql->select("*")
                ->from("m_soal")
                ->where("=", "id_materi", $models[$key]['id'])
//                        ->andWhere("=", "id_kelompok", $models[$key]['id_kelompok'])
                ->andWhere("=", "level", 3);
        $susah = $sqlsusah->count();

        if ($models[$key]['cara_penilaian'] == 1) {
            $models[$key]['penilaian'] = 'Singel Answer';
        } else if ($models[$key]['cara_penilaian'] == 2) {
            $models[$key]['penilaian'] = 'Essay';
        } else {
            $models[$key]['penilaian'] = 'Skala';
        }

        $models[$key]['mudah'] = $mudah;
        $models[$key]['sedang'] = $sedang;
        $models[$key]['susah'] = $susah;
    }

    $sql->clearQuery();

    echo json_encode(array('status' => 1, 'data' => $models, 'totalItems' => $totalItem), JSON_PRETTY_PRINT);
});
post('/appmateri/create', function() {
    check_access(array('admin' => true));
    $params = json_decode(file_get_contents("php://input"), true);
    $sql = new LandaDb();
    $data = $params;
    $data['is_delete'] = 0;


    if (validasiMateri($data) === true) {

        $model = $sql->insert("m_materi", $data);
        if ($model) {
            echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
        } else {
            echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
        }
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiMateri($data)), JSON_PRETTY_PRINT);
    }
});

post('/appmateri/update', function() {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $params = json_decode(file_get_contents("php://input"), true);

    $data = $params;

    if (validasiMateri($data) === true) {
        $model = $sql->update('m_materi', $params, array('id' => $params['id']));
        $model = $sql->run("UPDATE ujian_det SET cara_penilaian = '$params[cara_penilaian]',nilai_benar = '$params[nilai_benar]',nilai_salah = '$params[nilai_salah]' WHERE id_materi = '$params[id]'");
        if ($model) {
            echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
        }
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiMateri($data)), JSON_PRETTY_PRINT);
    }
});

del('/appmateri/delete/:id', function($id) {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $sql->delete('m_materi', array('id' => $id));
    echo json_encode(array('status' => 1));
});
