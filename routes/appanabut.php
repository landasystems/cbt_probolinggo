<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
get('/appanabut/listmateri', function() {
    $sql = new LandaDb();
    $models = $sql->findAll("select * from m_materi WHERE cara_penilaian = 1");

    foreach ($models as $key => $val) {
        $kelompok = $sql->find("SELECT * from m_kelompok where id='{$val->id_kelompok}'");
        $models[$key] = (array) $val;

        $models[$key]['nama_kelompok'] = "$kelompok->nama";
    }

    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

get('/appanabut/laporan/:id', function($id) {
    check_access(array('admin' => true));
    $params = json_decode(file_get_contents("php://input"), true);
    $sql = new LandaDb();

    $model = $sql->select("ujian.nama_ujian, ujian_det.id_materi, ujian_det.id_ujian")
            ->from("ujian_det")
            ->join("left join", "ujian", "ujian_det.id_ujian = ujian.id")
            ->where("=", "id_materi", $id)
            ->findAll();

    $noo = 1;
    $total = [];
    foreach ($model as $key => $val) {
        /*         * PENCARIAN NAMA MATERI* */
        $materi = $sql->select("nama_materi")
                ->from("m_materi")
                ->where("=", "id", $val->id_materi)
                ->orderBy("id ASC")
                ->find();

        /*         * PENCARIAN LIST SOAL* */
        $listSoal = $sql->select("id,jawaban")
                ->from("m_soal")
                ->where("=", "id_materi", $val->id_materi)
                ->orderBy("id ASC")
                ->findAll();

        /** PENCARIAN LIST PESERTA* */
        $listPeserta = $sql->select("m_peserta.nama,m_peserta.no_ujian,m_peserta.j_kelamin, tes.peserta_id")
                ->from("tes")
                ->join("left join", "m_peserta", "m_peserta.id = tes.peserta_id")
                ->where("=", "tes.ujian_id", $val->id_ujian)
                ->orderBy("m_peserta.nama ASC")
                ->findAll();

        foreach ($listPeserta as $keyPeserta => $valPeserta) {
            $pesertaId = $valPeserta->peserta_id;
            $listJawaban = $sql->select("tes_det.jawaban,tes_det.jawaban_pilih, tes_det.id_soal")
                    ->from("tes_det")
                    ->join("left join", "tes", "tes.id = tes_det.tes_id")
                    ->where("=", "tes.peserta_id", "$pesertaId")
                    ->andWhere("=", "tes_det.cara_penilaian", "1")
                    ->orderBy("tes_det.id_soal ASC")
                    ->findAll();

            foreach ($listJawaban as $keyJawaban => $valJawaban) {
                $listJawaban[$keyJawaban] = (array) $valJawaban;
                if ($valJawaban->jawaban == $valJawaban->jawaban_pilih) {
                    $listJawaban[$keyJawaban]['keterangan'] = 1;
                    $total[$valJawaban->id_soal]['benar'] = (isset($total[$valJawaban->id_soal]['benar']) ? $total[$valJawaban->id_soal]['benar'] : 0 ) + 1;
                } else {
                    $listJawaban[$keyJawaban]['keterangan'] = 0;
                    $total[$valJawaban->id_soal]['salah'] = (isset($total[$valJawaban->id_soal]['salah']) ? $total[$valJawaban->id_soal]['salah'] : 0 ) + 1;
                }
            }

            $listPeserta[$keyPeserta] = (array) $valPeserta;
            $listPeserta[$keyPeserta]['no'] = $noo++;
            $listPeserta[$keyPeserta]['listJawaban'] = $listJawaban;
        }

        $no = 1;
        foreach ($listSoal as $keySoal => $valSoal) {
            $listSoal[$keySoal] = (array) $valSoal;
            $listSoal[$keySoal]['no'] = $no++;

            if (isset($total[$valSoal->id])) {
                $listSoal[$keySoal]['benar'] = isset($total[$valSoal->id]['benar']) ? $total[$valSoal->id]['benar'] : 0;
                $listSoal[$keySoal]['salah'] = isset($total[$valSoal->id]['salah']) ? $total[$valSoal->id]['salah'] : 0;
            } else {
                $listSoal[$keySoal]['benar'] = 0;
                $listSoal[$keySoal]['salah'] = 0;
            }

            $listSoal[$keySoal]['total'] = $listSoal[$keySoal]['benar'] + $listSoal[$keySoal]['salah'];
            $listSoal[$keySoal]['persenBenar'] = round($listSoal[$keySoal]['benar'] / $listSoal[$keySoal]['total'] * 100, 0);
            $listSoal[$keySoal]['persenSalah'] = round($listSoal[$keySoal]['salah'] / $listSoal[$keySoal]['total'] * 100, 0);
        }

        $model[$key] = (array) $val;
        $model[$key]['soal'] = [
            "nama_materi" => $materi->nama_materi,
            "listSoal" => $listSoal
        ];
        $model[$key]['peserta'] = $listPeserta;
    }
    echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
});