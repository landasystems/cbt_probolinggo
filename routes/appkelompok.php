<?php

function validasiKelompok($data, $custom = array()) {
    $validasi = array(
        'nama' => 'required',
    );

    $cek = cek_validate($data, $validasi, $custom);
    return $cek;
}

get('/appkelompok/index', function() {

    check_access(array('admin' => true));

    //init variable
    $params = $_REQUEST;
    $filter = array();
    $sort = "id ASC";
    $offset = 0;
    $limit = 10;

    //limit & offset pagination
    if (isset($params['limit']))
        $limit = $params['limit'];
    if (isset($params['offset']))
        $offset = $params['offset'];

    //sorting
    if (isset($params['sort'])) {
        $sort = $params['sort'];
        if (isset($params['order'])) {
            if ($params['order'] == "false")
                $sort.=" ASC";
            else
                $sort.=" DESC";
        }
    }

    $sql = new LandaDb();
    $sql->select("*")
            ->from('m_kelompok')
            ->limit($limit)
            ->orderBy($sort)
            ->offset($offset);

    //filter
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        $query_filter = [];

        foreach ($filter as $key => $val) {
            if ($key == 'nama') {
                $sql->andWhere("LIKE", "nama", $val);
            }
        }
        $_SESSION['filter'] = $params['filter'];
    } else {
        $_SESSION['filter'] = '';
    }

    $models = $sql->findAll();


    $totalItems = $sql->count();
    $sql->clearQuery();

    echo json_encode(array('status' => 1, 'data' => $models, 'totalItems' => $totalItems), JSON_PRETTY_PRINT);
});

post('/appkelompok/create', function() {

    check_access(array('admin' => true));
    $sql = new LandaDb();
    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;

    if (validasiKelompok($data) === true) {
        $model = $sql->insert('m_kelompok', $params);
        if ($model) {
            echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
        }
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiKelompok($data)), JSON_PRETTY_PRINT);
    }
});


post('/appkelompok/update', function() {

    check_access(array('admin' => true));
    $params = json_decode(file_get_contents("php://input"), true);
    $sql = new LandaDb();
    $data = $params;

    if (validasiKelompok($data) === true) {
        $model = $sql->update('m_kelompok', $params, array('id' => $params['id']));
        if ($model) {
            echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
        }
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiKelompok($data)), JSON_PRETTY_PRINT);
    }
});

del('/appkelompok/delete/:id', function($id) {

    check_access(array('admin' => true));

    $sql = new LandaDb();

    $sql->delete('m_kelompok', array('id' => $id));
    echo json_encode(array('status' => 1));
});
