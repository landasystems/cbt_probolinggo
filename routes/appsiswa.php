<?php

function validasiSiswa($data, $custom = array())
{
    $validasi = array(
        'nama' => 'required',
        'NIS' => 'required',
        'no_ujian' => 'required',
        'j_kelamin' => 'required',
        'tempat_lahir' => 'required',
        'tanggal_lahir' => 'required',
    );

    $cek = cek_validate($data, $validasi, $custom);
    return $cek;
}

get('/appsiswa/getKelas', function () {
    check_access(array('admin' => true));

    $sql = new LandaDb();
    $models = $sql->select("*")
        ->from("m_kelompok")
        ->findAll();

    if ($models) {
        echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400), JSON_PRETTY_PRINT);
    }
});


get('/appsiswa/session', function () {
    print_r($_SESSION['param']);
});
get('/appsiswa/index', function () {

    check_access(array('admin' => true));

    //init variable
    $params = $_REQUEST;
    $filter = array();
    $sort = "id DESC";
    $offset = 0;
    $limit = 10;

    //limit & offset pagination
    if (isset($params['limit']))
        $limit = $params['limit'];
    if (isset($params['offset']))
        $offset = $params['offset'];

    //sorting
    if (isset($params['sort'])) {
        $sort = $params['sort'];
        if (isset($params['order'])) {
            if ($params['order'] == "false")
                $sort .= " ASC";
            else
                $sort .= " DESC";
        }
    }


    $sql = new LandaDb();
    $sql->select("m_siswa.*,m_kelompok.nama as namaKelas")
        ->from('m_siswa')
        ->join("left join", "m_kelompok", "m_kelompok.id = m_siswa.id_kelompok")
        ->limit($limit)
        ->orderBy($sort)
        ->offset($offset);


    //filter
    $where = '';
    if (isset($params['filter'])) {
        $_SESSION['param'] = $params['filter'];
        $filter = (array)json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            $sql->where('LIKE', $key, $val);
        }
    }

    $totalItem = $sql->count();


//    $sql->log();
    $models = $sql->findAll();

    foreach ($models as $key => $val) {
        $models[$key] = (array)$val;

//        $kelas = $sql->select("*")->from("m_kelompok")->where("=", "id", $val->id_kelompok)->find();
//
//        $models[$key]['namaKelas'] = $kelas->nama;
    }


    $sql->clearQuery();

    echo json_encode(array('status' => 1, 'data' => (array)$models, 'totalItems' => $totalItem), JSON_PRETTY_PRINT);
});

post('/appsiswa/create', function () {
    check_access(array('admin' => true));
    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;

    $sql = new LandaDb();

    if (validasiSiswa($data) === true) {

        $model = $sql->insert('m_siswa', $data);
        if ($model) {
            echo json_encode(array('status' => 1, 'data' => (array)$model), JSON_PRETTY_PRINT);
        }
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiSiswa($data)), JSON_PRETTY_PRINT);
    }
});

post('/appsiswa/saveImport', function () {
    check_access(array('admin' => true));
    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;

    $sql = new LandaDb();

    try {
        foreach ($data as $val) {
            $time = strtotime("$val[tanggal_lahir]");

            $val['tanggal_lahir'] = date('Y-m-d', $time);
            $val['is_delete'] = 0;
            $model = $sql->insert('m_siswa', $val);
        }
        echo json_encode(array('status' => 1, 'data' => (array)$model), JSON_PRETTY_PRINT);
    } catch (Exception $e) {
        echo json_encode(array('status' => 0, 'data' => 'Terjadi Kesalahan Saat Proses Penyimpanan'), JSON_PRETTY_PRINT);
//        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
    }

});

post('/appsiswa/update', function () {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $params = json_decode(file_get_contents("php://input"), true);

    $data = $params;

    if (validasiSiswa($data) === true) {
        $model = $sql->update('m_siswa', $params, array('id' => $data['id']));
        if ($model) {
            echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
        }
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiSiswa($data)), JSON_PRETTY_PRINT);
    }
});

del('/appsiswa/delete/:id', function ($id) {

    check_access(array('admin' => true));

    $sql = new LandaDb();
    $model = $sql->delete('m_siswa', array('id' => $id));
    echo json_encode(array('status' => 1));
});

post('/appsiswa/importexcel/:kelompok_id', function ($kelompok_id) {
    if (!empty($_FILES)) {
        $tempPath = $_FILES['file']['tmp_name'];
        $newName = urlParsing($_FILES['file']['name']);

        $uploadPath = "./excel/" . $newName;
        move_uploaded_file($tempPath, $uploadPath);

        if (is_file($uploadPath)) {

//            $inputFileName = 'file/' . 'format_import_siswa.xlsx';
            $inputFileName = $uploadPath;

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $query = '';

            for ($row = 1; $row <= $highestRow; $row++) {
                if ($row >= 3) {


                    $data['NIS'] = $objPHPExcel->getSheet(0)->getCell('B' . $row)->getValue();
                    $data['no_ujian'] = $objPHPExcel->getSheet(0)->getCell('C' . $row)->getValue();
                    $data['nama'] = $objPHPExcel->getSheet(0)->getCell('D' . $row)->getValue();
                    $data['j_kelamin'] = strtoupper($objPHPExcel->getSheet(0)->getCell('E' . $row)->getValue());
                    $data['tempat_lahir'] = $objPHPExcel->getSheet(0)->getCell('F' . $row)->getValue();
                    $data['tanggal_lahir'] = $objPHPExcel->getSheet(0)->getCell('G' . $row)->getValue();
                    $data['id_kelompok'] = $kelompok_id;
                    $data_import[] = $data;


                }
            }

            echo json_encode($data_import);
            unlink($uploadPath);
        }
    }
});
get('/appsiswa/exportexcel', function () {
    $sql = new LandaDb();
    $sql->select("*")
        ->from('m_siswa');

    //filter
//    $where = '';
    if (isset($_SESSION['param'])) {
        $filter = (array)json_decode($_SESSION['param']);
        foreach ($filter as $key => $val) {
            $sql->where('LIKE', $key, $val);
        }
    }

    $models = $sql->findAll();

//    foreach ($models as $val) {
////        echo $val->nama;
//    }
//    echo json_encode($models, true);
//    if (!isset($_GET['print'])) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=excel-rekap-peserta-ujian.xls");
//    }
    ?>
    <table width="100%" style="border-collapse: collapse; " border="1">
        <thead>
        <tr>
            <th colspan="7">Badan Kepegawaian Daerah Sidoarjo<br/>Data Peserta Ujian</th>
        </tr>
        <tr>
            <th>No.</th>
            <th>NIP</th>
            <th>Nama</th>
            <th>Nomor Ujian</th>
            <th>Janis Kelamin</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $no = 0;
        foreach ($models as $val) {
            $no++;
            ?>
            <tr>
                <td style="text-align: center"><?= $no ?></td>
                <td><?= $val->nik ?></td>
                <td><?= $val->nama ?></td>
                <td><?= $val->no_ujian ?></td>
                <td><?= $val->j_kelamin ?></td>
                <td><?= $val->tempat_lahir ?></td>
                <td><?= date("m F Y", strtotime($val->tanggal_lahir)); ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
});


post("/appsiswa/upload", function () {
    check_access(array('login' => true));
    include 'lib/excel-reader/reader.php';

    if (!empty($_FILES)) {
        $tempPath = $_FILES['file']['tmp_name'];
        $newName = urlParsing($_FILES['file']['name']);

        $uploadPath = "./excel" . DIRECTORY_SEPARATOR . $newName;
        move_uploaded_file($tempPath, $uploadPath);

        if (is_file($uploadPath)) {
            $excel = new Spreadsheet_Excel_Reader();

            $excel->read($uploadPath);

            $x = 4;
            $array = [];
            while ($x <= $excel->sheets[0]['numRows']) {
                $array[$x]['id'] = '';
                $array[$x]['nama'] = isset($excel->sheets[0]['cells'][$x][2]) ? $excel->sheets[0]['cells'][$x][2] : '';
                $array[$x]['no_ujian'] = isset($excel->sheets[0]['cells'][$x][3]) ? $excel->sheets[0]['cells'][$x][3] : '';
                $array[$x]['j_kelamin'] = isset($excel->sheets[0]['cells'][$x][4]) ? $excel->sheets[0]['cells'][$x][4] : '';
                $array[$x]['tempat_lahir'] = isset($excel->sheets[0]['cells'][$x][5]) ? $excel->sheets[0]['cells'][$x][5] : '';
                $array[$x]['tanggal_lahir'] = isset($excel->sheets[0]['cells'][$x][6]) ? $excel->sheets[0]['cells'][$x][6] : '';
                $x++;
            }

            $no = 0;
            $hasil = [];
            $sql = new LandaDb();
            foreach ($array as $val) {
                $hasil[$no] = $val;


                $model = $sql->insert('m_siswa', $hasil[$no]);
                $no++;
            }
        } else {
            $hasil = [];
        }

        if (!empty($hasil)) {
            unlink($uploadPath);
        }

        echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'data' => "error"), JSON_PRETTY_PRINT);
    }
});
