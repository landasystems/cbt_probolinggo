<?php

function validasiSoal($data, $custom = array())
{
    if ($data['type_soal'] == 2) {
        $validasi = array(
            'id_materi' => 'required',
            'level' => 'required',
            'pertanyaan' => 'required',
            'jawaban' => 'required',
        );
    } elseif ($data['type_soal'] == 3) {
        $validasi = array(
            'soal_cerita' => 'required',
        );
    } else {
        $validasi = array(
            'id_materi' => 'required',
            'level' => 'required',
            'pertanyaan' => 'required',
            'jawaban' => 'required',
            'jawaban_a' => 'required',
            'jawaban_b' => 'required',
        );
    }

    $cek = cek_validate($data, $validasi, $custom);
    return $cek;
}

get('/appsoal/getCerita/:id', function ($id) {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $models = $sql->select("*")
        ->from("m_soal_cerita")
        ->where("=", "id", $id)
        ->find();

    echo json_encode(array('status' => 1, 'data' => $models));
});
get('/appsoal/getSoalCerita/:id', function ($id) {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $models = $sql->select("*")
        ->from("m_soal")
        ->where("=", "id_soal_cerita", $id)
        ->orderBy("id ASC")
        ->findAll();
    $no = 1;
    foreach ($models as $key => $val) {
        $models[$key] = (array)$val;
        $models[$key]['no'] = $no;
        $no++;
    }

//    --------===========----------

    $soal_cerita = $sql->select("*")
        ->from("m_soal_cerita")
        ->where("=", "id", $id)
        ->find();
    echo json_encode(array('status' => 1, 'data' => $models,'cerita'=> $soal_cerita));
});

get('/appsoal/listnomor/:id_materi/:level', function ($id_materi, $level) {
    $sql = new LandaDb();
    $sql->select("*")
        ->from('m_soal')
        ->andWhere("=", "id_materi", $id_materi)
        ->andWhere("=", "level", $level)
        ->orderBy("id ASC");

    $models = $sql->findAll();

    $print = $sql;
    $print->customWhere("id_soal_cerita IS NULL", "AND");

    $data_print = $print->findAll();
    $_SESSION['exportSoal'] = $data_print;
//    $sq = $sql->findAll("SELECT count(*) as jumlah, id, id_soal_cerita, type_soal FROM m_soal WHERE id_materi = '$id_materi' AND level = '$level' GROUP BY type_soal");
    // print_r($models);
    $id_cerita_sudah = [];
    $hasil = [];
    $i = 0;
    foreach ($models as $key => $val) {
        if ($val->id_soal_cerita > 0) {
            //jika soal cerita DAN soal cerita yang belum pernah ada
            if (!in_array($val->id_soal_cerita, $id_cerita_sudah)) {
                $no = '';
                foreach ($models as $key_2 => $val_2) {
                    // dicari disemua soal dengan id cerita yang sama
                    if ($val->id_soal_cerita == $val_2->id_soal_cerita) {
                        // jika sama, nomor akan digabung
                        $i += 1;
                        if (!empty($no)) {
                            $no .= ', ';
                        }

                        $no .= $i;
                    }
                }

                $hasil[$key] = (array)$val;
                $hasil[$key]['no'] = $no;
                $hasil[$key]['cssstyle'] = "btn-primary";
                $id_cerita_sudah[] = $val->id_soal_cerita;
                $hasil[$key]['pertanyaan'] = '';
                $hasil[$key]['jawaban_a'] = '';
                $hasil[$key]['jawaban_b'] = '';
                $hasil[$key]['jawaban_c'] = '';
                $hasil[$key]['jawaban_d'] = '';
                $hasil[$key]['jawaban_e'] = '';
                $model_soal_cerita = $sql->select("*")->from("m_soal_cerita")->where("=","id",$val->id_soal_cerita)->find();
                $hasil[$key]['soal_cerita'] = $model_soal_cerita->soal_cerita;
            }
        } else {
            $i += 1;
            $hasil[$key] = (array)$val;
            $hasil[$key]['no'] = $i;
            $hasil[$key]['cssstyle'] = "btn-primary";
            // $hasil[$key]['pertanyaan'] = str_replace("&", " ", $val->pertanyaan);
            $hasil[$key]['pertanyaan'] = $val->pertanyaan;
//            $hasil[$key]['pertanyaan'] = '';
//            $hasil[$key]['jawaban_a'] = '';
//            $hasil[$key]['jawaban_b'] = '';
//            $hasil[$key]['jawaban_c'] = '';
//            $hasil[$key]['jawaban_d'] = '';
//            $hasil[$key]['jawaban_e'] = '';

        }
    }
//
    $caraPenilaian = $sql->select("cara_penilaian,type_jawaban")->from("m_materi")->where("=", "id", $id_materi)->find();
    echo json_encode(array('status' => 1, 'data' => $hasil, 'cara_penilaian' => $caraPenilaian->cara_penilaian, 'type_jawaban' => $caraPenilaian->type_jawaban));
});

get('/appsoal/index/:id_materi/:level', function ($id_materi, $level) {
    check_access(array('admin' => true));

//init variable
    $params = $_REQUEST;
    $filter = array();
    $sort = "id ASC";
    $offset = 0;
    $limit = 10;

//limit & offset pagination
    if (isset($params['limit'])) {
        $limit = $params['limit'];
    }

    if (isset($params['offset'])) {
        $offset = $params['offset'];
    }

//sorting
    if (isset($params['sort'])) {
        $sort = $params['sort'];
        if (isset($params['order'])) {
            if ($params['order'] == "false") {
                $sort .= " ASC";
            } else {
                $sort .= " DESC";
            }
        }
    }

    $sql = new LandaDb();
    $sql->select("*")
        ->from('m_soal')
//            ->where("=", "id_kelompok", $id_kelompok)
        ->andWhere("=", "id_materi", $id_materi)
        ->andWhere("=", "level", $level)
        ->limit($limit)
        ->orderBy($sort)
        ->offset($offset);

    $print = $sql;

//    if ($id != 0) {
    //        $sql->where("=", "id_kelompok", $id);
    //    }
    //filter

    if (isset($params['filter'])) {
        $filter = (array)json_decode($params['filter']);
        $query_filter = [];

        foreach ($filter as $key => $val) {
            if ($key == 'nama') {
                $sql->andWhere("LIKE", "nama", $val);
            }
        }
        $_SESSION['filter'] = $params['filter'];
    } else {
        $_SESSION['filter'] = '';
    }

    $models = $sql->findAll();
    $i = 0;
    foreach ($models as $key => $val) {
        $i = $i + 1;
        $models[$key] = (array)$val;
        $models[$key]['no'] = $i;
    }

    $totalItems = $sql->count();
    $sql->clearQuery();

    $cara_penilaian = $sql->select("cara_penilaian")
        ->from("m_materi")
        ->where("=", "id", $id_materi)
        ->find();


    $_SESSION['exportSoal'] = $models;

    echo json_encode(array('status' => 1, 'data' => $models, 'totalItems' => $totalItems, 'cara_penilaian' => $cara_penilaian->cara_penilaian), JSON_PRETTY_PRINT);
});

post('/appsoal/create', function () {

    check_access(array('admin' => true));
    $sql = new LandaDb();
    $params = json_decode(file_get_contents("php://input"), true);

    if ($params['form']['type_soal'] == 3) {
        if (validasiSoal($params['form']) === true) {
            $soal_cerita = $params['form']['soal_cerita'];
//            $cekIdSoalCerita = $sql->select("id")->from("m_soal_cerita")->orderBy("id DESC")->limit("1")->find();
//            if ($cekIdSoalCerita == FALSE) {
//                $idd = 1;
//            } else {
//                $idd = $cekIdSoalCerita->id + 1;
//            }
//            echo $cekIdSoalCerita;
//            $soal_cerita = $sql->run("INSERT INTO m_soal_cerita VALUES(NULL ,'$soal_cerita')");
            $model_soal_cerita = $sql->insert("m_soal_cerita",["soal_cerita" => $soal_cerita]);

            foreach ($params['detail'] as $val) {
                $val['id_soal_cerita'] = $model_soal_cerita->id;
                $val['type_soal'] = $params['form']['type_soal'];
                $detail = $sql->insert('m_soal', $val);
            }
            echo json_encode(array('status' => 1, 'data' => $soal_cerita), JSON_PRETTY_PRINT);
        } else {
            echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiSoal($params['form'])), JSON_PRETTY_PRINT);
        }
    } else {
        if (validasiSoal($params['form']) === true) {
            $model = $sql->insert('m_soal', $params['form']);
            if ($model) {
                echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
            }
        } else {
            echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiSoal($params)), JSON_PRETTY_PRINT);
        }
    }
});

post('/appsoal/deleteSemua', function () {
    check_access(array('admin' => true));
    $params = json_decode(file_get_contents("php://input"), true);
    $sql = new LandaDb();

    foreach ($params[detail] as $val) {
        $models = $sql->run("DELETE FROM m_soal WHERE id = '$val[id]'");
    }
    $hapusCerita = $sql->run("DELETE FROM m_soal_cerita WHERE id = '$params[idSoalCerita]'");
//
    if ($hapusCerita) {
        echo json_encode(array('status' => 1, 'data' => $hapusCerita), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400), JSON_PRETTY_PRINT);
    }
});
post('/appsoal/update', function () {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $params = json_decode(file_get_contents("php://input"), true);

    if ($params['form']['type_soal'] == 3) {
        if (validasiSoal($params['form']) === true) {
            $soal_cerita = $params['form']['soal_cerita'];
            $idCerita = $params[detail][0][id_soal_cerita];
            $soal_cerita = $sql->run("UPDATE m_soal_cerita SET soal_cerita = '$soal_cerita' WHERE id = '$idCerita'");

            foreach ($params['detail'] as $val) {
                $val['type_soal'] = $params['form']['type_soal'];
                $detail = $sql->update('m_soal', $val, array('id' => $val['id']));
            }
        } else {
            echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiSoal($params['form'])), JSON_PRETTY_PRINT);
        }
    } else {
        if (validasiSoal($params['form']) === true) {
            $idBiasa = $params['form']['id'];
            $model = $sql->update('m_soal', $params['form'], array('id' => $idBiasa));
            if ($model) {
                echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
            }
        } else {
            echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiSoal($params)), JSON_PRETTY_PRINT);
        }
    }


//    check_access(array('admin' => true));
//    $params = json_decode(file_get_contents("php://input"), true);
//    $sql = new LandaDb();
//    $data = $params;
//
//    if (validasiSoal($data) === true) {
//        $model = $sql->update('m_soal', $data, array('id' => $data['id']));
//        if ($model) {
//            echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
//        }
//    } else {
//        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => validasiSoal($data)), JSON_PRETTY_PRINT);
//    }
});

del('/appsoal/delete/:id', function ($id) {
    check_access(array('admin' => true));

    $sql = new LandaDb();

    $sql->delete('m_soal', array('id' => $id));
    echo json_encode(array('status' => 1));
});

get('/appsoal/exportexcel/:id_materi/:level', function ($id_materi, $level) {
    $sql = new LandaDb();
    $sql->select("*")
        ->from('m_soal')
        ->where("=", "id_materi", $id_materi)
        ->andWhere("=", "level", $level);

//filter
    //    $where = '';
    if (isset($_SESSION['param'])) {
        $filter = (array)json_decode($_SESSION['param']);
        foreach ($filter as $key => $val) {
            $sql->andWhere('LIKE', $key, $val);
        }
    }
    $nama_materi = $sql->find("SELECT nama_materi FROM m_materi WHERE id = $id_materi");
    if ($level == '1') {
        $levels = 'Mudah';
    } elseif ($level == '2') {
        $levels = 'Sedang';
    } elseif ($level == '3') {
        $levels = 'Sulit';
    }
    $models = $sql->findAll();
//    foreach ($models as $val) {
    ////        echo $val->nama;
    //    }
    //    echo json_encode($models, true);
    //    if (!isset($_GET['print'])) {
    //    header("Content-type: application/vnd-ms-excel");
    //    header("Content-Disposition: attachment; filename=excel-rekap-soal-$nama_materi->nama_materi-$levels.xls");
    //    }
    ?>
    <table width="100%" style="border-collapse: collapse; " border="1">
        <tbody>
        <tr>
            <th>No.</th>
            <th>Pertanyaan</th>
            <th>Jawaban_a</th>
            <th>Jawaban_b</th>
            <th>Jawaban_c</th>
            <th>Jawaban_d</th>
            <th>Jawaban_e</th>
            <th>Jawaban_benar</th>
        </tr>
        <?php
        $no = 0;
        foreach ($models as $val) {
            $no++;
            ?>

            <tr>
                <td style="text-align: center"><?= $no ?></td>
                <td><?= $val->pertanyaan ?></td>
                <td><?= $val->jawaban_a ?></td>
                <td><?= $val->jawaban_b ?></td>
                <td>
                    <?php
                    if ($val->jawaban_c == !null) {
                        echo "$val->jawaban_c";
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($val->jawaban_d == !null) {
                        echo "$val->jawaban_d";
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($val->jawaban_e == !null) {
                        echo "$val->jawaban_e";
                    }
                    ?>
                </td>
                <td>
                    <?= $val->jawaban ?>
                </td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>
    <?php
});

post("/appsoal/upload/:id/:level", function ($id, $level) {
    check_access(array('login' => true));
    include 'lib/excel-reader/reader.php';

    if (!empty($_FILES)) {
        $tempPath = $_FILES['file']['tmp_name'];
        $newName = urlParsing($_FILES['file']['name']);

        $uploadPath = "./excel" . DIRECTORY_SEPARATOR . $newName;
        move_uploaded_file($tempPath, $uploadPath);

//    $uploadPath = "./excel/format-cbt-soal.xls";
        if (is_file($uploadPath)) {
            $excel = new Spreadsheet_Excel_Reader();

            $excel->read($uploadPath);

            $x = 2;
            $array = [];
            $soal_cerita = [];
            $cerita = [];
            while ($x <= $excel->sheets[0]['numRows']) {


                if (strtolower($excel->sheets[0]['cells'][$x][9]) == 'ya') {
                    $nomor_soal_cerita = $excel->sheets[0]['cells'][$x][10];
                    $nomor_soal = $excel->sheets[0]['cells'][$x][1];

                    $soal_cerita[$nomor_soal_cerita][$nomor_soal]['pertanyaan'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][2]) ? $excel->sheets[0]['cells'][$x][2] : ''), $arr_remove);
                    $soal_cerita[$nomor_soal_cerita][$nomor_soal]['jawaban_a'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][3]) ? $excel->sheets[0]['cells'][$x][3] : ''), $arr_remove);
                    $soal_cerita[$nomor_soal_cerita][$nomor_soal]['jawaban_b'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][4]) ? $excel->sheets[0]['cells'][$x][4] : ''), $arr_remove);
                    $soal_cerita[$nomor_soal_cerita][$nomor_soal]['jawaban_c'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][5]) ? $excel->sheets[0]['cells'][$x][5] : ''), $arr_remove);
                    $soal_cerita[$nomor_soal_cerita][$nomor_soal]['jawaban_d'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][6]) ? $excel->sheets[0]['cells'][$x][6] : ''), $arr_remove);
                    $soal_cerita[$nomor_soal_cerita][$nomor_soal]['jawaban_e'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][7]) ? $excel->sheets[0]['cells'][$x][7] : ''), $arr_remove);
                    $soal_cerita[$nomor_soal_cerita][$nomor_soal]['jawaban'] = strtolower(strtr(json_encode(isset($excel->sheets[0]['cells'][$x][8]) ? $excel->sheets[0]['cells'][$x][8] : ''), $arr_remove));
                } else {
                    $array[$x]['id_materi'] = "$id";
                    $array[$x]['level'] = "$level";
                    $arr_remove = ['\\u0000' => '', '"' => ''];
                    $array[$x]['pertanyaan'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][2]) ? $excel->sheets[0]['cells'][$x][2] : ''), $arr_remove);
                    $array[$x]['jawaban_a'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][3]) ? $excel->sheets[0]['cells'][$x][3] : ''), $arr_remove);
                    $array[$x]['jawaban_b'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][4]) ? $excel->sheets[0]['cells'][$x][4] : ''), $arr_remove);
                    $array[$x]['jawaban_c'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][5]) ? $excel->sheets[0]['cells'][$x][5] : ''), $arr_remove);
                    $array[$x]['jawaban_d'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][6]) ? $excel->sheets[0]['cells'][$x][6] : ''), $arr_remove);
                    $array[$x]['jawaban_e'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][7]) ? $excel->sheets[0]['cells'][$x][7] : ''), $arr_remove);
                    $array[$x]['jawaban'] = strtolower(strtr(json_encode(isset($excel->sheets[0]['cells'][$x][8]) ? $excel->sheets[0]['cells'][$x][8] : ''), $arr_remove));
                }

                if ($excel->sheets[0]['cells'][$x][12] != '') {
                    $cerita[$excel->sheets[0]['cells'][$x][12]]['cerita'] = strtr(json_encode(isset($excel->sheets[0]['cells'][$x][13]) ? $excel->sheets[0]['cells'][$x][13] : ''), $arr_remove);
                }

                $x++;
            }

            $sql = new LandaDb();
            foreach ($cerita as $key => $val) {
                $cerita[$key] = (array)$val;
                $cekIdSoalCerita = $sql->select("id")->from("m_soal_cerita")->orderBy("id DESC")->limit("1")->find();
                if ($cekIdSoalCerita == FALSE) {
                    $idd = 1;
                } else {
                    $idd = $cekIdSoalCerita->id + 1;
                }

                $insert_cerita = $sql->run("INSERT INTO m_soal_cerita VALUES($idd ,'$val[cerita]')");

                foreach ($soal_cerita[$key] as $val) {
                    $val['id_soal_cerita'] = $idd;
                    $val['type_soal'] = 3;
                    $val['id_materi'] = $id;
                    $val['level'] = $level;
                    $detail = $sql->insert('m_soal', $val);
                }
                $cerita[$key]['detail'] = $soal_cerita[$key];
            }
            $no = 0;
            $hasil = [];

            foreach ($array as $val) {
                $hasil[$no] = $val;
                if ($val['pertanyaan'] != '') {
                    if ($val['jawaban_e'] == '') {
                        unset($val['jawaban_e']);
                    }
                    $model = $sql->insert('m_soal', $hasil[$no]);
                    $no++;
                }
            }
//            echo json_encode($cerita);
        } else {
            $hasil = [];
            echo "ora enek file";
        }

        if (!empty($hasil)) {
            unlink($uploadPath);
        }

        echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'data' => "error"), JSON_PRETTY_PRINT);
    }
});

get('/appsoal/export', function () {
    /** Error reporting */
    error_reporting(E_ALL);
    ini_set('display_errors', true);
    ini_set('display_startup_errors', true);
    date_default_timezone_set('Europe/London');
    if (PHP_SAPI == 'cli') {
        die('This example should only be run from a Web Browser');
    }

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../lib/Classes/PHPExcel.php';
// Create new PHPExcel object
    $objPHPExcel = new PHPExcel();
// Set document properties
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");
// Add some data
    $no = 1;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A1", "No")
        ->setCellValue("B1", "Pertanyaan")
        ->setCellValue("C1", "Jawaban A")
        ->setCellValue("D1", "Jawaban B")
        ->setCellValue("E1", "Jawaban C")
        ->setCellValue("F1", "Jawaban D")
        ->setCellValue("G1", "Jawaban E")
        ->setCellValue("H1", "Jawaban benar (isi dengan a,b,c,d,e) (Jika ujian bertipe skala kosongkan saja)");
    foreach ($_SESSION['exportSoal'] as $val) {
        $no++;
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$no", $no - 1)
            ->setCellValue("B$no", "$val->pertanyaan")
            ->setCellValue("C$no", "$val->jawaban_a")
            ->setCellValue("D$no", "$val->jawaban_b")
            ->setCellValue("E$no", "$val->jawaban_c")
            ->setCellValue("F$no", "$val->jawaban_d")
            ->setCellValue("G$no", "$val->jawaban_e")
            ->setCellValue("H$no", "$val->jawaban");
    }
// Miscellaneous glyphs, UTF-8
    //    $objPHPExcel->setActiveSheetIndex(0)
    //            ->setCellValue('A4', 'Miscellaneous glyphs')
    //            ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');
    // Rename worksheet
    //    $objPHPExcel->getActiveSheet()->setTitle('Simple');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    //    $objPHPExcel->setActiveSheetIndex(0);
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="data_master_soal.xls"');
    header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
});
