<?php

function validasiUjian($data, $custom = array()) {
    $validasi = array(
        'nama_ujian' => 'required',
        'kode_ujian' => 'required',
        'pin_sesi' => 'required',
        'status' => 'required',
        'durasi' => 'required',
    );

    $cek = cek_validate($data, $validasi, $custom);
    return $cek;
}

function validasiDetail($data, $custom = array()) {
    $validasi = array(
        'materi' => 'required',
        'level' => 'required',
        'jumlah_soal' => 'required',
    );

    $cek = cek_validate($data, $validasi, $custom);
    return $cek;
}

get('/appujian/getKodeUjian/:val', function($val) {
    $sql = new LandaDb();
    $models = $sql->select("*")->from("ujian")->where("=", "kode_ujian", $val)->find();
    if ($models) {
        echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'data' => $models), JSON_PRETTY_PRINT);
    }
});

get('/appujian/listKelas', function() {
    $sql = new LandaDb();
    $models = $sql->findAll("select * from m_kelompok");

    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

get('/appujian/updatePeserta/:id', function($id) {
    $sql = new LandaDb();
    $models = $sql->select("*")->from("m_siswa")->where("=", "id_kelompok", $id)->findAll();

    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

get('/appujian/listmateri', function() {
    $sql = new LandaDb();
    $sql->select("m_materi.*")
            ->from("m_materi")
            ->andWhere("=", "is_aprove", 1)
            ->join("left join", "m_user", "m_user.id = m_materi.created_by");
    if ($_SESSION['user']['roles_id'] != 0) {
        $sql->andWhere("=", "m_materi.created_by", $_SESSION['user']['id']);
    } 
     $models = $sql->findAll();
//    $models = $sql->findAll("select * from m_materi where is_aprove = 1");
//    $models = $sql->findAll("select * from m_materi");

    foreach ($models as $key => $val) {
        $kelompok = $sql->find("SELECT * from m_kelompok where id='{$val->id_kelompok}'");
        $models[$key] = (array) $val;

        $models[$key]['nama_kelompok'] = "$kelompok->nama";

//        echo '<pre>';
//        print_r($kelompok);
    }

    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

get('/appujian/jumlah_soal/:id/:level', function($id, $level) {
    $sql = new LandaDb();
    $sql->select("*")
            ->from("m_soal")
            ->where("=", "id_materi", $id)
            ->andWhere("=", "level", $level)
            ->findAll();

    $jumlah = $sql->count();

    echo json_encode(array('status' => 1, 'data' => $jumlah), JSON_PRETTY_PRINT);
});

get('/appujian/listdetail/:id', function($id) {
    $sql = new LandaDb();
    $list = $sql->select("*")
            ->from('ujian_det')
            ->where("=", "id_ujian", $id);
    $models = $list->findAll();
    foreach ($models as $key => $val) {
        $akun = $sql->find("select * from m_materi where id='{$val->id_materi}'");
        $models[$key] = (array) $val;
        $models[$key]['materi'] = $akun;

        $kelompok = $sql->find("SELECT * from m_kelompok where id='{$akun->id_kelompok}'");
        $models[$key]['materi']->nama_kelompok = "$kelompok->nama";
    }



    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});
get('/appujian/listdetail2/:id', function($id) {
    $sql = new LandaDb();
    $list = $sql->select("*")
            ->from('m_peserta')
            ->where("=", "ujian_id", $id);
    $models = $list->findAll();
//    foreach ($models as $key => $val) {
//        $akun = $sql->find("select * from m_materi where id='{$val->id_materi}'");
//        $models[$key] = (array) $val;
//        $models[$key]['materi'] = $akun;
//
//        $kelompok = $sql->find("SELECT * from m_kelompok where id='{$akun->id_kelompok}'");
//        $models[$key]['materi']->nama_kelompok = "$kelompok->nama";
//    }
    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

get("/appujian/view/:id", function($id) {
    $sql = new LandaDb();
    $models = $sql->findAll("select * from ujian_det where id_ujian='{$id}'");
    foreach ($models as $key => $val) {
        $akun = $sql->find("select * from m_materi where id='{$val->id_materi}'");
        $models[$key] = (array) $val;
        $models[$key]['materi'] = $akun;
    }
    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

get('/appujian/index', function() {

    check_access(array('admin' => true));

    $params = $_REQUEST;
    $filter = array();
    $sort = "id DESC";
    $offset = 0;
    $limit = 10;

//limit & offset pagination
    if (isset($params['limit']))
        $limit = $params['limit'];
    if (isset($params['offset']))
        $offset = $params['offset'];

//sorting
    if (isset($params['sort'])) {
        $sort = $params['sort'];
        if (isset($params['order'])) {
            if ($params['order'] == "false")
                $sort .= " ASC";
            else
                $sort .= " DESC";
        }
    }

    $sql = new LandaDb();
    $sql->select("ujian.*, m_user.nama as dibuat_oleh")
            ->from("ujian")
            ->join("left join", "m_user", "m_user.id = ujian.created_by");
    if ($_SESSION['user']['roles_id'] != 0) {
        $sql->andWhere("=", "ujian.created_by", $_SESSION['user']['id']);
    }
            $sql->limit($limit)
            ->orderBy($sort)
            ->offset($offset);

//filter
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            $sql->where('LIKE', $key, $val);
        }
    }
    $totalItem = $sql->count();
    $models = $sql->findAll();

    foreach ($models as $key => $val) {
        $models[$key] = (array) $val;
        if ($models[$key]['status'] == 1) {
            $models[$key]['statuss'] = 'Aktif';
        } else {
            $models[$key]['statuss'] = 'Tidak Aktif';
        }

        if ($models[$key]['created_by'] != 0) {
            $id_user = $models[$key]['created_by'];
            $nama_user = $sql->find("select * from m_user where id = '$id_user'");

            $models[$key]['created_by'] = $nama_user->nama;
        }
    }

    $sql->clearQuery();

    echo json_encode(array('status' => 1, 'data' => $models, 'totalItems' => $totalItem), JSON_PRETTY_PRINT);
});
post('/appujian/create', function() {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $params = json_decode(file_get_contents("php://input"), true);
//    $id_awal = $sql->find("select * from ujian order by id desc LIMIT 1");
////    $id = (empty($id_awal)) ? 1 : $id_awal->id + 1;
//    if (empty($id_awal)) {
//        $id = 1;
//    } else {
//        $id = $id_awal + 1;
//    }
    $data = $params;

    if (validasiUjian($data['form']) === true && validasiDetail($data['detail']['0']) === TRUE) {
        $cekKodeUjian = $sql->select("*")->from("ujian")->where("=", "kode_ujian", $data['form']['kode_ujian'])->find();
        if ($cekKodeUjian == FALSE) {
            $cekPinSesi = $sql->select("*")->from("ujian")->where("=", "pin_sesi", $data['form']['pin_sesi'])->find();
            if ($cekPinSesi == FALSE) {
                $data['form']['id'] = $id;
                $model = $sql->insert("ujian", $data['form']);
                foreach ($data['detail'] as $key => $value) {
                    $detail[$key] = $value;
                    $detail[$key]['id_ujian'] = $model->id;
                    $detail[$key]['id_materi'] = $value['materi']['id'];
                    $detail[$key]['cara_penilaian'] = $value['materi']['cara_penilaian'];
                    $model_detail = $sql->insert("ujian_det", $detail[$key]);
                }

                foreach ($data['detail2'] as $key => $value) {
                    $detail2[$key] = $value;
                    $detail2[$key]['ujian_id'] = $model->id;
                    unset($detail2[$key]['id']);
                    $model_detail = $sql->insert("m_peserta", $detail2[$key]);
                }

                echo json_encode(array('status' => 1, 'data' => (array) $model, 'id' => $id), JSON_PRETTY_PRINT);
            } else {
                echo json_encode(array('status' => 3, 'error_code' => 400, 'errors' => "PIN Sesi Sudah terpakai"), JSON_PRETTY_PRINT);
            }
        } else {
            echo json_encode(array('status' => 2, 'error_code' => 400, 'errors' => "Kode Ujian Sudah terpakai"), JSON_PRETTY_PRINT);
        }
    } else {
        $error1 = validasiUjian($data['form']) . '<br>';
        $error2 = validasiDetail($data['detail']['0']);
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => $error1 . $error2), JSON_PRETTY_PRINT);
    }
});

post('/appujian/update', function() {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $params = json_decode(file_get_contents("php://input"), true);

    $data = $params;

    if (validasiUjian($data['form']) === true && validasiDetail($data['detail']['0']) === TRUE) {
        $detail = [];
        $idSession = $sql->select("*")->from("m_user")->where("=", "nama", $data['form']['created_by'])->find();
        $data['form']['created_by'] = $idSession->id;
        $model = $sql->update('ujian', $data['form'], array('id' => $data['form']['id']));
//        $sql->delete('m_peserta', array('ujian_id' => $data['form']['id']));

        if ($model) {
            foreach ($data['detail'] as $key => $value) {
                $detail[$key] = (array) $value;
                $detail[$key]['id_ujian'] = $data['form']['id'];
                $detail[$key]['id_materi'] = $value['materi']['id'];
                $detail[$key]['cara_penilaian'] = $value['materi']['cara_penilaian'];
                if (!empty($value['id'])) {
                    $model_detail = $sql->update("ujian_det", $detail[$key], array('id' => $value['id']));
                } else {
                    $model_detail = $sql->insert("ujian_det", $detail[$key]);
                }
            }

            $detail2 = [];
            foreach ($data['detail2'] as $key => $value) {
                $detail2[$key] = (array) $value;
                $detail2[$key]['ujian_id'] = $data['form']['id'];
                if (!empty($value['id'])) {
                    $model_detail = $sql->update("m_peserta", $detail2[$key], array('id' => $value['id']));
                } else {
                    $model_detail = $sql->insert("m_peserta", $detail2[$key]);
                }
            }
            echo json_encode(array('status' => 1, 'dataForm' => $model, 'dataDetail' => (array) $model_detail), JSON_PRETTY_PRINT);
        } else {
            echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan', 'hhh' => $data), JSON_PRETTY_PRINT);
        }
    } else {
        $error1 = validasiUjian($data['form']) . '<br>';
        $error2 = validasiDetail($data['detail']['0']);
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => $error1 . $error2), JSON_PRETTY_PRINT);
    }
});

del('/appujian/deleteDetail/:id', function($id) {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $sql->delete('ujian_det', array('id' => $id));
    echo json_encode(array('status' => 1));
});
del('/appujian/deletePeserta/:id', function($id) {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $sql->delete('m_peserta', array('id' => $id));
    echo json_encode(array('status' => 1));
});
del('/appujian/delete/:id', function($id) {
    check_access(array('admin' => true));
    $sql = new LandaDb();
    $sql->delete('m_peserta', array('ujian_id' => $id));
    $sql->delete('ujian_det', array('id_ujian' => $id));
    $sql->delete('ujian', array('id' => $id));
    echo json_encode(array('status' => 1));
});


post("/appujian/upload", function() {
    check_access(array('login' => true));
    include 'lib/excel-reader/reader.php';

    if (!empty($_FILES)) {
        $tempPath = $_FILES['file']['tmp_name'];
        $newName = urlParsing($_FILES['file']['name']);

        $uploadPath = "./excel" . DIRECTORY_SEPARATOR . $newName;
        move_uploaded_file($tempPath, $uploadPath);

        if (is_file($uploadPath)) {
            $excel = new Spreadsheet_Excel_Reader();

            $excel->read($uploadPath);

            $x = 2;
            $array = [];
            while ($x <= $excel->sheets[0]['numRows']) {
                $array[$x]['id'] = '';

                $array[$x]['nama'] = json_decode(str_replace('\\u0000', '', json_encode(isset($excel->sheets[0]['cells'][$x][2]) ? $excel->sheets[0]['cells'][$x][2] : '')));
                $array[$x]['no_ujian'] = json_decode(str_replace('\\u0000', '', json_encode(isset($excel->sheets[0]['cells'][$x][3]) ? $excel->sheets[0]['cells'][$x][3] : '')));
                $array[$x]['j_kelamin'] = json_decode(str_replace('\\u0000', '', json_encode(isset($excel->sheets[0]['cells'][$x][4]) ? $excel->sheets[0]['cells'][$x][4] : '')));
                $array[$x]['tempat_lahir'] = json_decode(str_replace('\\u0000', '', json_encode(isset($excel->sheets[0]['cells'][$x][5]) ? $excel->sheets[0]['cells'][$x][5] : '')));
                $array[$x]['tanggal_lahir'] = json_decode(str_replace('\\u0000', '', json_encode(isset($excel->sheets[0]['cells'][$x][6]) ? $excel->sheets[0]['cells'][$x][6] : '')));
                $x++;
            }

            $no = 0;
            $hasil = [];
            foreach ($array as $val) {
                if ($val['nama'] != '') {
                    $hasil[$no] = $val;
                    $no++;
                }
            }
        } else {
            $hasil = [];
        }

        if (!empty($hasil)) {
            unlink($uploadPath);
        }

        echo json_encode(array('status' => 1, 'data' => (array) $hasil), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'data' => "error"), JSON_PRETTY_PRINT);
    }
});
