<?php

get('/appguru/index', function() {

    check_access(array('admin' => true));

    //init variable
    $params = $_REQUEST;
    $filter = array();
    $sort = "id DESC";
    $offset = 0;
    $limit = 10;

    //limit & offset pagination
    if (isset($params['limit']))
        $limit = $params['limit'];
    if (isset($params['offset']))
        $offset = $params['offset'];

    //sorting
    if (isset($params['sort'])) {
        $sort = $params['sort'];
        if (isset($params['order'])) {
            if ($params['order'] == "false")
                $sort.=" ASC";
            else
                $sort.=" DESC";
        }
    }

    $sql = new LandaDb();
    $sql->select("*")
            ->from('m_user')
            ->where("=", "roles_id", 1)
            ->limit($limit)
            ->orderBy($sort)
            ->offset($offset)
            ;

    //filter
    $where = '';
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            $sql->andWhere('LIKE', $key, $val);
        }
    }
//    $sql->log();
    $models = $sql->findAll();
    $totalItem = $sql->count();
    $sql->clearQuery();

    echo json_encode(array('status' => 1, 'data' => (array) $models, 'totalItems' => $totalItem), JSON_PRETTY_PRINT);
});

post('/appguru/create', function() {

    check_access(array('admin' => true));

    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;
    
    $data['roles_id'] = '1';
    $data['password'] = sha1($data['password']);

    $sql = new LandaDb();
    $model = $sql->insert('m_user', $data);

    if ($model) {
        echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
    }
});

post('/appguru/update', function($id) {

    check_access(array('admin' => true));

    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;
   

    if (!empty($params['password'])) {
        $data['password'] = sha1($data['password']);
    } else {
        unset($data['password']);
    }

    $sql = new LandaDb();
    $model = $sql->update('m_user', $data, array('id' => $params['id']));

    if ($model) {
        echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
    }
});

post('/appguru/updateprofile', function() {

    check_access(array('admin' => true));

    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;
    
    if (!empty($params['password'])) {
        $data['password'] = sha1($data['password']);
    } else {
        unset($data['password']);
    }

    $sql = new LandaDb();
    $model = $sql->update('m_user', $data, array('id' => $_SESSION['user']['id']));

    if ($model) {
        echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
    }
});

get('/appguru/view/:id', function($id) {

    check_access(array('admin' => true));

    $sql = new LandaDb2();
    $model = $sql->find("select * from emp where nik=$id");
    unset($model->password);
    echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
});

get('/appguru/profile', function() {

    check_access(array('admin' => true));

    $id = $_SESSION['user']['id'];
    $sql = new LandaDb();
    $model = $sql->find("select * from m_user where id = '$id'");
    unset($model->password);

    echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
});

del('/appguru/delete/:id', function($id) {

    check_access(array('admin' => true));

    $sql = new LandaDb();
    $model = $sql->delete('m_user', array('id' => $id));
    echo json_encode(array('status' => 1));
});
