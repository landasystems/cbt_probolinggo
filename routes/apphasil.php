<?php

get('/apphasil/listkode_ujian', function() {
    $sql = new LandaDb();
    $list = $sql->findAll("select * from ujian WHERE type_penilaian != 'intensif_stan'");

    echo json_encode(array('status' => 1, 'data' => $list), JSON_PRETTY_PRINT);
});
get('/apphasil/kode_ujian', function() {
    $sql = new LandaDb();
    $list = $sql->findAll("select * from ujian");

    echo json_encode(array('status' => 1, 'data' => $list), JSON_PRETTY_PRINT);
});


get('/apphasil/view/:id', function($id) {
    $sql = new LandaDb();
    $list = $sql->select("*")
            ->from('ujian')
            ->where("=", "id", $id);
    $models = $list->find();

    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

post('/apphasil/laporan', function() {
    check_access(array('admin' => true));
    $hasil = [];
    $params = json_decode(file_get_contents("php://input"), true);
    $sql = new LandaDb();
//    $kelompok = '';
    $kolom = '';
    $no = 0;

    $ujian = $sql->select("*")
            ->from("ujian")
            ->where("=", "id", $params['kode_ujian']['id'])
            ->find();

    $peserta = $sql->select("tes.*,m_peserta.nama,no_ujian,m_peserta.asal_sekolah")
            ->from("tes")
            ->join("inner join", "m_peserta", "m_peserta.id = tes.peserta_id")
            ->where("=", "tes.ujian_id", $params['kode_ujian']['id'])
            ->findAll();
    $passing = $sql->select("passing_grade,type_penilaian")
            ->from("ujian")
            ->where("=", "id", $params['kode_ujian']['id'])
            ->find();
    $passing_grade = $passing->passing_grade;
    $type_penilaian = $passing->type_penilaian;

    if ($params['mode'] == 'materi') {
        foreach ($peserta as $key => $val_peserta) {
            $no++;
//            tabel th
            $sql->select("ujian_det.*, m_materi.id_kelompok, m_materi.nama_materi, m_kelompok.nama")
                    ->from("ujian_det")
                    ->join("INNER JOIN", "m_materi", "m_materi.id = ujian_det.id_materi")
                    ->join("INNER JOIN", "m_kelompok", "m_kelompok.id = m_materi.id_kelompok")
                    ->where("=", "id_ujian", $params['kode_ujian']['id']);

            $model_tabel = $sql->findAll();
//            $kolom = $model_tabel;
//perhitungan nilai
            $hasil[$key] = (array) $val_peserta;

            $tes_det = $sql->select("tes_det.*,  m_materi.nama_materi")
                    ->from("tes_det")
                    ->join("INNER JOIN", "m_materi", "m_materi.id = tes_det.materi_id")
                    ->where("=", "tes_id", $val_peserta->id)
                    ->orderBy('nomor ASC')
                    ->findAll();
            $summary = summary_tes($tes_det);
            $total_materi = 0;
            $materi = [];
            foreach ($model_tabel as $val) {

                if ($val->cara_penilaian == 1) { //single answer
                    $nilai = (isset($summary['nilai_materi'][$val->id])) ? $summary['nilai_materi'][$val->id] : 0;
                } elseif ($val->cara_penilaian == 2) { //essay
                    $nilai = (isset($summary['nilai_essay'][$val->id])) ? $summary['nilai_essay'][$val->id] : 0;
                } else { //skala
                    $nilai = (isset($summary['jwb_materi'][$val->id])) ? $summary['jwb_materi'][$val->id] : 0;
                }


                $materi[$val->id]['id'] = $val->id_materi;
                $materi[$val->id]['peserta_id'] = $val_peserta->peserta_id;

                $materi[$val->id]['nama'] = $val->nama_materi;
                $materi[$val->id]['id_kelompok'] = $val->id_kelompok;
                $materi[$val->id]['nama_kelompok'] = $val->nama;
                $materi[$val->id]['cara_penilaian'] = $val->cara_penilaian;
                $materi[$val->id]['nilai'] = $nilai;
                $total_materi += $materi[$val->id]['nilai'];


                $hasil[$key]['no'] = $no;
//                $hasil[$key]['materi'] = $materi;
                $hasil[$key]['nilai_total'] = $total_materi;
                $hasil[$key]['asal_sekolah'] = $val_peserta->asal_sekolah;

//                passing grade
                if ($type_penilaian == 'intensif_stan') {
                    $benar_minimal = $summary['total'] / 3;
                    if ($summary['jwb_benar'] >= $benar_minimal) {
                        $keterangan = $ujian->passing_grade_iya;
                    } else {
                        $keterangan = $ujian->passing_grade_tidak;
                    }
                } else {
                    if ($total_materi >= $passing_grade) {
                        $keterangan = $ujian->passing_grade_iya;
                    } else {
                        $keterangan = $ujian->passing_grade_tidak;
                    }
                }

                $hasil[$key]['passing_grade'] = $keterangan;
            }

            foreach ($materi as $val) {

                $idPeserta = $val['peserta_id'];
                $kelompok[$val['id']]['id'] = $val['id'];
                $kelompok[$val['id']]['peserta_id'] = $val['peserta_id'];
                $kelompok[$val['id']]["nilai"][$idPeserta] = (isset($kelompok[$val['id']]["nilai"][$idPeserta])) ? $kelompok[$val['id']]["nilai"][$idPeserta] : 0;
                $kelompok[$val['id']]['nama_materi'] = $val['nama'];
                $kelompok[$val['id']]['cara_penilaian'] = $val['cara_penilaian'];
                $kelompok[$val['id']]["nilai"][$idPeserta] += $val['nilai'];


                $hasil[$key]['materi'] = $kelompok;
            }
//            echo '-----';
//            print_r($kelompok);
            $kolom = $hasil[$key]['materi'];
        }



//        echo base64_encode($array);
//        $hasil = array_sort($hasil, 'nilai_total', SORT_DESC);
    } elseif ($params['mode'] == 'kelompok') {
        foreach ($peserta as $key => $val_peserta) {
            //            tabel th
            $no++;
            $sql->select("ujian_det.*, m_materi.id_kelompok, m_materi.nama_materi, m_kelompok.nama")
                    ->from("ujian_det")
                    ->join("INNER JOIN", "m_materi", "m_materi.id = ujian_det.id_materi")
                    ->join("INNER JOIN", "m_kelompok", "m_kelompok.id = m_materi.id_kelompok")
                    ->where("=", "id_ujian", $params['kode_ujian']['id']);

            $model_tabel = $sql->findAll();

//            perhitungan nilai

            $hasil[$key] = (array) $val_peserta;
            $tes_det = $sql->select("tes_det.*,  m_materi.nama_materi")
                    ->from("tes_det")
                    ->join("INNER JOIN", "m_materi", "m_materi.id = tes_det.materi_id")
                    ->where("=", "tes_id", $val_peserta->id)
                    ->orderBy('nomor ASC')
                    ->findAll();
            $summary = summary_tes($tes_det);
            $total_materi = 0;
            $materi = [];
            foreach ($model_tabel as $val) {

                if ($val->cara_penilaian == 1) { //single answer
                    $nilai = (isset($summary['nilai_materi'][$val->id])) ? $summary['nilai_materi'][$val->id] : 0;
                } else { //skala
                    $nilai = (isset($summary['jwb_materi'][$val->id])) ? $summary['jwb_materi'][$val->id] : 0;
                }

                $materi[$val->id]['nama'] = $val->nama_materi;
                $materi[$val->id]['id_kelompok'] = $val->id_kelompok;
                $materi[$val->id]['nama_kelompok'] = $val->nama;
                $materi[$val->id]['nilai'] = $nilai;
                $total_materi += $materi[$val->id]['nilai'];

//                $hasil[$key]['materi'] = $materi;
                $hasil[$key]['no'] = $no;
                $hasil[$key]['nilai_total'] = $total_materi;

//                penentuan passing grade
                if ($type_penilaian == 'intensif_stan') {
                    $benar_minimal = $summary['total'] / 3;
                    if ($summary['jwb_benar'] >= $benar_minimal) {
                        $keterangan = $ujian->passing_grade_iya;
                    } else {
                        $keterangan = $ujian->passing_grade_tidak;
                    }
                } else {
                    if ($total_materi >= $passing_grade) {
                        $keterangan = $ujian->passing_grade_iya;
                    } else {
                        $keterangan = $ujian->passing_grade_tidak;
                    }
                }
                $hasil[$key]['passing_grade'] = $keterangan;
            }

            //mencari nilai per kelompok
//            $kelom= [];
            foreach ($materi as $val) {
                $kelompok[$val['id_kelompok']]['id'] = $val['id_kelompok'];
                $kelompok[$val['id_kelompok']]['nilai'] = (isset($kelompok[$val['id_kelompok']]['nilai'])) ? $kelompok[$val['id_kelompok']]['nilai'] : 0;
                $kelompok[$val['id_kelompok']]['nama_materi'] = $val['nama_kelompok'];
                $kelompok[$val['id_kelompok']]['nilai'] += $val['nilai'];

                $hasil[$key]['materi'] = $kelompok;
            }
            $kolom = $hasil[$key]['materi'];
//            foreach ($hasil[$key]['materi'] as $key => $val) {
//                $kolom[$val['id']] = $val;
//            }
        }
    }

//    echo json_encode($hasil);
//    print_r($hasil);
    $_SESSION['ujianExport'] = $hasil;
    echo json_encode(array('status' => 1, 'kolom' => $kolom, 'peserta' => $hasil, 'ujian' => $ujian), JSON_PRETTY_PRINT);
});


get('/apphasil/export', function() {
    /** Error reporting */
    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    date_default_timezone_set('Europe/London');
    if (PHP_SAPI == 'cli')
        die('This example should only be run from a Web Browser');
    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../lib/Classes/PHPExcel.php';
// Create new PHPExcel object
    $objPHPExcel = new PHPExcel();
// Set document properties
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
// Add some data
    $no = 2;
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A1", "No")
            ->setCellValue("B1", "Nama")
            ->setCellValue("C1", "No Ujian")
            ->setCellValue("D1", "Nilai")
            ->setCellValue("E1", "Keterangan");
    foreach ($_SESSION['ujianExport'] as $val) {
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A$no", $no - 1)
                ->setCellValue("B$no", "$val[nama]")
                ->setCellValue("C$no", "$val[no_ujian]")
                ->setCellValue("D$no", "$val[nilai_total]")
                ->setCellValue("E$no", "$val[passing_grade]");
        $no++;
    }

// Miscellaneous glyphs, UTF-8
//    $objPHPExcel->setActiveSheetIndex(0)
//            ->setCellValue('A4', 'Miscellaneous glyphs')
//            ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');
// Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Laporan Hasil');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="data_laporan_hasil_ujian.xls"');
    header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
});
