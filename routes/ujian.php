<?php

get('/ujian/session', function () {

//    check_access(array('admin' => true));

    $models = $_SESSION['peserta'];

    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

get('/ujian/tes', function () {
    //    check_access(array('admin' => true));

    $sql = new LandaDb();
    $tes = $sql->select("*")
        ->from("tes")
        ->where("=", "peserta_id", $_SESSION['peserta']['id'])
        ->andWhere("=", "ujian_id", $_SESSION['peserta']['ujian_id'])
        ->find();


    if (empty($tes)) {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Tidak ditemukan tes Anda, diharuskan login dulu'));
    } else {
        $ujian = $sql->select("*")
            ->from("ujian")
            ->andWhere("=", "id", $_SESSION['peserta']['ujian_id'])
            ->find();

        $tes->durasi = ($tes->time_selesai - $tes->time_mulai) / 60;
        $tes->counter = $tes->time_selesai - time();
        $tes_det = $sql->select("tes_det.*, m_materi.nama_materi, m_soal_cerita.soal_cerita")
            ->from("tes_det")
            ->join("INNER JOIN", "m_materi", "m_materi.id = tes_det.materi_id")
            ->join("LEFT JOIN", "m_soal_cerita", "m_soal_cerita.id = tes_det.id_soal_cerita")
            ->where("=", "tes_id", $tes->id)
            ->orderBy('nomor ASC')
            ->findAll();

        $tes->summary = summary_tes($tes_det);

        foreach ($tes_det as $key => $val) {
            $models[$key] = (array)$val;

            $models[$key]['pertanyaan'] = str_replace("<input", "<img", $val->pertanyaan);
            $models[$key]['soal_cerita'] = str_replace("<input", "<img", $val->soal_cerita);
        }

//        echo '<pre>';
//        print_r($models);

        if ($tes->status == 0) {
            echo json_encode(array('status' => -1, 'tes' => $tes, 'tes_det' => $tes_det, 'error_code' => 400, 'errors' => 'Ujian telah diakhiri, Anda tidak bisa melakukan ujian ulang'));
        } else if (!empty($ujian) && $ujian->status == 0) { //jika status ujian sudah di non aktifkan
            echo json_encode(array('status' => -2, 'error_code' => 400, 'errors' => 'Ujian telah di Non Aktifkan, Harap hubungi administrator'));
        } else {
            echo json_encode(array('status' => 1, 'tes' => $tes, 'tes_det' => $models));
        }
    }
});

post('/ujian/jawab', function () {
//    check_access(array('admin' => true));
//    echo 'aaaaaaaaa';
    $sql = new LandaDb();
    $params = json_decode(file_get_contents("php://input"), true);
    $model = $sql->update('tes_det', ['jawaban_pilih' => $params['jawaban_pilih']], ['id' => $params['id']]);

    if ($model) {
        echo json_encode(array('status' => 1));
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Terjadi kesalahan'));
    }
});

post('/ujian/selesai', function () {
//    check_access(array('admin' => true));
    $sql = new LandaDb();
    $params = json_decode(file_get_contents("php://input"), true);
    $model = $sql->update('tes', ['status' => 0], ['id' => $_SESSION['peserta']['tes_id']]);

    $tes_det = $sql->select("tes_det.*, m_materi.nama_materi")
        ->from("tes_det")
        ->join("INNER JOIN", "m_materi", "m_materi.id = tes_det.materi_id")
        ->where("=", "tes_id", $_SESSION['peserta']['tes_id'])
        ->orderBy('nomor ASC')
        ->findAll();
    $passing = $sql->select("passing_grade, passing_grade_iya, passing_grade_tidak,type_penilaian")
        ->from("ujian")
        ->where("=", "id", $_SESSION['peserta']['ujian_id'])
        ->find();
    $summary = summary_tes($tes_det,$passing->type_penilaian);

    $ujian_det = $sql->select("ujian_det.*, m_materi.id_kelompok, m_materi.nama_materi, m_kelompok.nama")
        ->from("ujian_det")
        ->join("INNER JOIN", "m_materi", "m_materi.id = ujian_det.id_materi")
        ->join("INNER JOIN", "m_kelompok", "m_kelompok.id = m_materi.id_kelompok")
        ->where("=", "id_ujian", $_SESSION['peserta']['ujian_id'])
        ->findAll();


    $passing_grade = $passing->passing_grade;
    $passing_grade_iya = $passing->passing_grade_iya;
    $passing_grade_tidak = $passing->passing_grade_tidak;
    $type_penilaian = $passing->type_penilaian;

    $total_materi = 0;
    $materi = $kelompok = [];
    foreach ($ujian_det as $val) {

        if ($val->cara_penilaian == 1) { //single answer
            $nilai = (isset($summary['nilai_materi'][$val->id])) ? round($summary['nilai_materi'][$val->id], 2) : 0;
            $nilai = ($nilai > 0) ? $nilai : 0;
        } elseif ($val->cara_penilaian == 2) { //essay
            $nilai = (isset($summary['nilai_essay'][$val->id])) ? round($summary['nilai_essay'][$val->id], 2) : 0;
            $nilai = ($nilai > 0) ? $nilai : 0;
        } else { //skala
            $nilai = (isset($summary['jwb_materi'][$val->id])) ? round($summary['jwb_materi'][$val->id], 2) : 0;
            $nilai = ($nilai > 0) ? $nilai : 0;
        }

        $materi[$val->id]['nama'] = $val->nama_materi;
        $materi[$val->id]['cara_penilaian'] = $val->cara_penilaian;
        $materi[$val->id]['id_kelompok'] = $val->id_kelompok;
        $materi[$val->id]['nama_kelompok'] = $val->nama;
        $materi[$val->id]['nilai'] = $nilai;
        $total_materi += $materi[$val->id]['nilai'];
    }

    //mencari nilai per kelompok
    foreach ($materi as $val) {
        $kelompok[$val['id_kelompok']]['nilai'] = (isset($kelompok[$val['id_kelompok']]['nilai'])) ? $kelompok[$val['id_kelompok']]['nilai'] : 0;
        $kelompok[$val['id_kelompok']]['nama'] = $val['nama_kelompok'];
        $kelompok[$val['id_kelompok']]['nilai'] += $val['nilai'];
    }

    if ($type_penilaian == 'intensif_stan') {
        $benar_minimal = $summary['total'] / 3;
        if ($summary['jwb_benar'] >= $benar_minimal) {
            $keterangan = $passing_grade_iya;
        } else {
            $keterangan = $passing_grade_tidak;
        }
    } else {
        if ($total_materi >= $passing_grade) {
            $keterangan = $passing_grade_iya;
        } else {
            $keterangan = $passing_grade_tidak;
        }
    }

    if ($model) {
        echo json_encode(array('status' => 1, 'materi' => $materi, 'total_materi' => round($total_materi, 2), 'kelompok' => $kelompok, 'keterangan' => $keterangan));
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Terjadi kesalahan'));
    }
});
