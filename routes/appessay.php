<?php

function validasiSiswa($data, $custom = array()) {
    $validasi = array(
        'nama' => 'required',
    );

    $cek = cek_validate($data, $validasi, $custom);
    return $cek;
}

get('/appessay/getUjian', function() {
    check_access(array('admin' => true));

    $sql = new LandaDb();
    $models = $sql->select("*")
            ->from("ujian")
            ->findAll();

    if ($models) {
        echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400), JSON_PRETTY_PRINT);
    }
});
get('/appessay/getJawaban/:id', function($id) {
    check_access(array('admin' => true));

    $sql = new LandaDb();
    $models = $sql->select("*")
            ->from("tes_det")
            ->where("=", "tes_id", $id)
            ->andWhere("=", "cara_penilaian", 2)
            ->findAll();

    if ($models) {
        echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400), JSON_PRETTY_PRINT);
    }
});

get('/appessay/updateEssay/:id/:isi', function($id, $isi) {
    check_access(array('admin' => true));

    $sql = new LandaDb();
    $cariNilai = $sql->select("nilai_benar")->from("tes_det")->where("=", "id", $id)->find();

    if ($isi == 1) {
        $nilai = $cariNilai->nilai_benar;
    } elseif ($isi == 2) {
        $nilai = $cariNilai->nilai_benar / 2;
    } else {
        $nilai = 0;
    }

    $models = $sql->run("UPDATE tes_det SET essay_koreksi = '$isi', essay_nilai = '$nilai' WHERE id = '$id'");
    if ($models) {
        echo json_encode(array('status' => 1, 'data' => $isi), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400), JSON_PRETTY_PRINT);
    }
});


get('/appessay/index', function() {

    check_access(array('admin' => true));

    //init variable
    $params = $_REQUEST;
    $filter = array();
    $sort = "m_peserta.nama ASC";
    $offset = 0;
    $limit = 50;

    //limit & offset pagination
    if (isset($params['limit']))
        $limit = $params['limit'];
    if (isset($params['offset']))
        $offset = $params['offset'];

    //sorting
    if (isset($params['sort'])) {
        $sort = $params['sort'];
        if (isset($params['order'])) {
            if ($params['order'] == "false")
                $sort .= " ASC";
            else
                $sort .= " DESC";
        }
    }

    $sql = new LandaDb();
    $sql->select("*, tes.id AS id")
            ->from('tes')
            ->join("left join", "m_peserta", "tes.peserta_id = m_peserta.id")
            ->where("=", "tes.ujian_id", $params['id_ujian'])
            ->limit($limit)
            ->orderBy($sort)
            ->offset($offset);

    //filter
    $where = '';
    if (isset($params['filter'])) {
        $_SESSION['param'] = $params['filter'];
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            $sql->where('LIKE', $key, $val);
        }
    }


//    $sql->log();
    $models = $sql->findAll();
    $no = 1;
    foreach ($models as $key => $val) {
        $models[$key] = (array) $val;

        $models[$key]['no'] = $no;
        $no++;
    }

    $totalItem = $sql->count();
    $sql->clearQuery();

    echo json_encode(array('status' => 1, 'data' => (array) $models, 'totalItems' => $totalItem), JSON_PRETTY_PRINT);
});
