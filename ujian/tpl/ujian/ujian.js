app.controller('ujianCtrl', function ($modal, $sce, $scope, Data, toaster, $state, $timeout) {
//    $scope.is_hasil = false;
    $scope.tampil = [];
    $scope.result = {};

    Data.get('site/getLogo').then(function (data) {
        $scope.desk = data.data;
    });

    Data.get('ujian/session').then(function (data) {
        $scope.session = data.data;
    });
    Data.get('ujian/tes').then(function (data) {
        if (data.status == 0) {
            $state.go("access.signin");
            return;
        } else if (data.status == -1) {
            $scope.counter = -1;
        } else if (data.status == -2) {
            $state.go("access.signin");
            return;
        } else {
            $scope.counter = data.tes.counter - 1;
        }

        $scope.tes = data.tes;
        $scope.tes_det = data.tes_det;
//        console.log($scope.tes_det[0]);
        $scope.update($scope.tes_det[0])

        $scope.counter_menit = Math.floor($scope.counter / 60);
        $scope.counter_detik = $scope.counter % 60;
        $scope.onTimeout = function () {
            $scope.counter--;

            //waktu habis dan tidak menekan tombol selesai
            if ($scope.counter < 0) {
                var data = {};
                Data.post('ujian/selesai', data).then(function (result) {
                    $scope.result.total_materi = result.total_materi;
                    $scope.result.materi = result.materi;
                    console.log(result.materi)
                    $scope.result.kelompok = result.kelompok;
                    $scope.result.keterangan = result.keterangan;
                    $scope.result.selesai = 1;
                })
                $scope.counter_menit = 0;
                $scope.counter_detik = 0;
                toaster.pop('success', "BERIKUT HASIL UJIAN ANDA", 'Ujian telah berakhir');
            } else {
                $scope.counter_detik = $scope.counter % 60;
                if ($scope.counter_detik == 0) {
                    $scope.counter_menit--;
                }
                mytimeout = $timeout($scope.onTimeout, 1000);
            }
        }
        var mytimeout = $timeout($scope.onTimeout, 1000);
    });

    $scope.update = function (form) {
        $scope.tampil = form;
        $scope.jawaban_pilih = form.jawaban_pilih;
        var text = form.pertanyaan;
        text = $sce.trustAsHtml(text);

        $scope.tampil.pertanyaan = text;
    }
    $scope.jawab = function (id) {
        //console.log($scope.jawaban_pilih);

        if (!$scope.tampil.jawaban_pilih) {
            $scope.tes.summary.total_jwb++;
            $scope.tes.summary.total_belum_jwb--;
        }

        var data = {
            id: id,
            jawaban_pilih: $scope.jawaban_pilih
        };
        $scope.tampil.jawaban_pilih = $scope.jawaban_pilih;
        Data.post('ujian/jawab', data).then(function (result) {
            if (result.status == 0) {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            } else {
                toaster.pop('success', "BERHASIL", 'Jawaban [' + $scope.jawaban_pilih + '] tersimpan');
            }
        })
    }

    $scope.selesai = function () {
        var modalInstance = $modal.open({
            templateUrl: 'tpl/ujian/modal.html',
            controller: 'modalCtrl',
            size: 'md',
            resolve: {
                result: function () {
                    return $scope.result;
                },
            }
        });



    };
});
app.controller('modalCtrl', function ($state, $scope, toaster, Data, $modalInstance, result) {
    $scope.resultmodal = result;

    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.selesai_ujian = function () {
        var data = {};
        Data.post('ujian/selesai', data).then(function (result) {
            $scope.resultmodal.total_materi = result.total_materi;
            $scope.resultmodal.materi = result.materi;
            $scope.resultmodal.kelompok = result.kelompok;
            $scope.resultmodal.keterangan = result.keterangan;
            $scope.resultmodal.selesai = 1;
        })

        $modalInstance.dismiss('cancel');
        toaster.pop('success', "BERIKUT HASIL UJIAN ANDA", 'Ujian telah berakhir');
    }


})