app.controller('siteCtrl', function ($scope, Data, toaster, $state) {
    $scope.authError = null;
    $scope.is_login = true;
//    $scope.is_cek = true;

    Data.get('site/getLogo').then(function (data) {
        $scope.desk = data.data;
    });

    $scope.login = function (form) {
        $scope.authError = null;

        Data.post('site/loginPeserta/', form).then(function (result) {
            if (result.status == 0) {
                $scope.authError = result.errors;
            } else {
                $state.go('site.ujian');
            }
        });
    };

    $scope.cek_biodata = function (form) {

        Data.post('site/cekBiodata/', form).then(function (result) {
            if (result.status == 0) {
                toaster.pop('error', "Peringatan", result.errors);
            } else {
                $scope.is_login = false;
                $scope.is_cek = true;
                $scope.valueCek = result.data;
            }
        });
    }

    $scope.batal = function () {
        $scope.is_cek = false;
        $scope.is_login = true;
        $scope.form = {};
        toaster.pop('warning', "Peringatan", "Ujian Telah di Batalkan");
    }
})
