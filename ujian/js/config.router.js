angular.module('app').run(
        ['$rootScope', '$state', '$stateParams', 'Data',
            function ($rootScope, $state, $stateParams, Data) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
                //pengecekan login
                $rootScope.$on("$stateChangeStart", function (event, toState) {
                    Data.get('site/sessionPeserta').then(function (results) {
                        if (typeof results.data.peserta != "undefined") {
                            $rootScope.user = results.data.peserta;
                        } else {
                            $state.go("access.signin");
                        }
                    });
                });
            }
        ]).config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {
                $urlRouterProvider.otherwise('/site/ujian');
                $stateProvider.state('site', {
                    abstract: true,
                    url: '/site',
                    templateUrl: 'tpl/app.html'
                }).state('site.ujian', {
                    url: '/ujian',
                    templateUrl: 'tpl/ujian/ujian.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/ujian/ujian.js');
                            }
                        ]
                    }
                }).state('site.coba', {
                    url: '/coba',
                    templateUrl: 'tpl/coba/coba.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/coba/coba.js');
                            }
                        ]
                    }
                })
                        // others
                        .state('access', {
                            url: '/access',
                            template: '<div ui-view class="fade-in-right-big smooth"></div>'
                        }).state('access.signin', {
                    url: '/signin',
                    templateUrl: 'tpl/page_signin.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/site/site.js').then();
                            }
                        ]
                    }
                }).state('access.404', {
                    url: '/404',
                    templateUrl: 'tpl/page_404.html'
                }).state('access.forbidden', {
                    url: '/forbidden',
                    templateUrl: 'tpl/page_forbidden.html'
                })
                        
            }
        ]);
